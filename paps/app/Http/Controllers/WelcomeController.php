<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use App\Repositories\ReservationRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;

class WelcomeController extends Controller
{
    private $product;
    private $reservation;

    public function __construct(ProductRepositoryInterface $product,
                                ReservationRepositoryInterface $reservation)
    {
        $this->product = $product;
        $this->reservation = $reservation;
    }

    public function index(){
        $guitar = $this->product->findBy("category","Guitar");
        $headset = $this->product->findBy("category","Headset");
        $reservation = $this->reservation->getAll();
        return view('welcome',[
            'guitars' => $guitar,
            'headsets' => $headset,
            'reservations' => $reservation
        ]);
    }

}
