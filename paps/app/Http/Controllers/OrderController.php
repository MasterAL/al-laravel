<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use App\Repositories\ProfileRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\OrderRepositoryInterface;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;

use App\Http\Requests;

class OrderController extends Controller
{
    private $order;
    private $paypal;
    private $product;

    public function __construct(OrderRepositoryInterface $order, ProductRepositoryInterface $productRepository)
    {
        $this->order = $order;
        $this->product = $productRepository;
        $this->paypal = new ApiContext(
            new OAuthTokenCredential(
                'AZjCLYJVewcNATtQOx9LHq_XnAFFSPlJkiU5OYWu9aezPKPXreKpb1GONHnxCRAyIXkUs1Qb7gJIE8OS',
                'EFLcxFT2m-DEVDgIgRqA5x_KbRHPkJLk40qLE6VHlRQUy7sPGtqxW1711S_v6Se8vi8FCefmv9A6wiZw'
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orders = $this->order->getAll();
        if ($request->has("ordernum")) {
            $ordernum = $request->input('ordernum');
            $orders = $this->order->findByWithUserAndProduct("ordernum", $ordernum);
        }
        return response()->json(['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->user()->id;
        if ($request->has("productid")) {
            $productid = $request->input('productid');
        }

        if ($request->has("payment")) {
            $payment = $request->input('payment');
        }

        $data = $this->order->create($id, $productid, 0,"Ongoing");

        $product = $this->product->findById($productid);



        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        if ($payment != "full"){
            $prize = $product->prize / 2;
        }else{
            $prize = $product->prize;
        }

        $item = new Item();
        $item->setName($product->name)
            ->setCurrency('PHP')
            ->setQuantity(1)
            ->setPrice($prize);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setSubtotal($prize);

        $amount = new Amount();
        $amount->setCurrency('PHP')
            ->setTotal($prize)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Knoxville')
            ->setInvoiceNumber($data->ordernum);

        $redirectUrls = new redirectUrls();
        $redirectUrls->setReturnUrl("http://giljan-001-site1.gtempurl.com/home")
            ->setCancelUrl("http://giljan-001-site1.gtempurl.com/home");

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);


        $product->stock = $product->stock - 1;

        $product->save();

        try {
            $payment->create($this->paypal);
        } catch (Exception $e) {
            die($e);
        }
          return response()->json(['status' => 'ok', 'link' =>  $payment->getApprovalLink()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ordernum)
    {
        if ($request->has("status")) {
            $status = $request->input('status');
            $this->order->updateStatus($ordernum, $status);
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
