<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'productid', 'customid','ordernum', 'change' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'productid');
    }

    public function custom()
    {
        return $this->belongsTo('App\Customize', 'customid');
    }

}
