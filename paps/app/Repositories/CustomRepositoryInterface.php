<?php

namespace App\Repositories;

interface CustomRepositoryInterface
{

    public function getAll();

    public function findById($id);

    public function findBy($att, $column);

    public function get($offset, $limit);

    public function update($id, $prize, $enable);
}
