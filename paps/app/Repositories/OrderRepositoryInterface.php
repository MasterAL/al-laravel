<?php

namespace App\Repositories;

interface OrderRepositoryInterface
{

    public function getAll();

    public function findById($id);

    public function findBy($att, $column);

    public function get($offset, $limit);

    public function create($userid, $productid, $customid, $status);

    public function findByWithUserAndProduct($column, $att);

    public function updateStatus($ordernum,$status);

}
