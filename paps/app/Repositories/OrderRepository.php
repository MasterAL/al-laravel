<?php

namespace App\Repositories;


use App\Order;
use App\Repositories\OrderRepositoryInterface;
use DB;

class OrderRepository implements OrderRepositoryInterface
{

    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getAll()
    {
        return $this->order->groupBy('ordernum')->with("user")->get();
    }

    public function findById($id)
    {
        return $this->order->find($id);
    }

    public function findBy($column, $att)
    {
        return $this->order->where($column, $att)->get();
    }

    public function findByWithUserAndProduct($column, $att)
    {
        return $this->order->where($column, $att)->with("user")->with("product")->with("custom")->get();
    }

    public function get($offset, $limit)
    {
        return $this->order->limit($limit)->offset($offset)->get();
    }

    public function create($userid, $productid, $customid, $status)
    {

        $order_date = date('Y/m/d');
        $dateElements = explode('/', $order_date);
        $count = DB::select(DB::raw("Select Count(Distinct ordernum) as count FROM `order` WHERE Date(created_at) = '$order_date'"));
        $order_num = $dateElements[0].$dateElements[1].$dateElements[2].sprintf("%02d", ($count[0]->count+1));

        $new_order = new Order();

        $new_order->userid = $userid;
        $new_order->productid = $productid;
        $new_order->customid = $customid;
        $new_order->ordernum = $order_num;
        $new_order->status = $status;

        $new_order->save();

        return $new_order;
    }

    public function updateStatus($ordernum,$status){
        $order = $this->order->where('ordernum', $ordernum)->update(['status'=>$status]);
        return $order;
    }
}