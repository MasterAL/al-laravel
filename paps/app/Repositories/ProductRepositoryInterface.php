<?php

namespace App\Repositories;

interface ProductRepositoryInterface{

    public function getAll();

    public function findById($id);

    public function delete($id);

    public function findBy($att, $column);

    public function get($offset,$limit);

    public function create($name, $prize, $pic, $category, $description, $stock);

    public function update($id, $name, $prize, $pic, $category, $stock, $description);

}