<?php

namespace App\Repositories;

use App\Custom;
use DB;

class CustomRepository implements CustomRepositoryInterface
{

    private $custom;

    public function __construct(Custom $custom)
    {
        $this->custom = $custom;
    }

    public function getAll()
    {
        return $this->custom->all();
    }

    public function findById($id)
    {
        return $this->custom->find($id);
    }

    public function findBy($column, $att)
    {
        return $this->custom->where($column, $att)->get();
    }

    public function get($offset, $limit)
    {
        return $this->custom->limit($limit)->offset($offset)->get();
    }

    public function update($id, $prize, $enable)
    {
        $custom = $this->custom->find($id);
        $custom->prize = $prize;
        $custom->enable = $enable;
        $custom->save();

    }
}