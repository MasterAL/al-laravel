<?php

namespace App\Repositories;

use App\Product;

class ProductRepository implements ProductRepositoryInterface
{

    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getAll()
    {
        return $this->product->all();
    }

    public function findById($id)
    {
        return $this->product->find($id);
    }

    public function findBy($column, $att)
    {
        return $this->product->where($column, $att)->get();
    }

    public function get($offset, $limit)
    {
        return $this->product->limit($limit)->offset($offset)->get();
    }

    public function create($name, $prize, $pic, $category, $description, $stock)
    {
        $product = new Product();

        $product->name = $name;
        $product->prize = $prize;
        $product->pic = $pic;
        $product->category = $category;
        $product->description = $description;
        $product->stock = $stock;

        $product->save();

        return $product;
    }

    public function delete($id)
    {
       return $this->product->destroy($id);
    }

    public function update($id, $name, $prize, $pic, $category, $stock, $description)
    {
        $product = $this->product->find($id);

        $product->name = $name;
        $product->prize = $prize;
        if ($pic != null){
            $product->pic = $pic;
        }
        $product->category = $category;
        if ($stock >= 10){
            $product->stock = 10;
        }else{
            $product->stock = $stock;
        }
        $product->description = $description;
        $product->save();


        return $product;
    }
}