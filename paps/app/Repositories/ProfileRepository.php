<?php

namespace App\Repositories;

use App\Profile;
use App\User;

class ProfileRepository implements ProfileRepositoryInterface
{

    private $profile;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    public function getAll()
    {
        return $this->profile->all();
    }

    public function findByUserId($id)
    {
        return $this->profile->where("userid", $id)->get()->first();
    }

    public function findBy($column, $att)
    {
        return $this->profile->where($column, $att)->get();
    }

    public function get($offset, $limit)
    {
        return $this->profile->limit($limit)->offset($offset)->get();
    }

    public function delete($id)
    {
        User::destroy($id);
        $profile = $this->profile->where("userid", $id)->get()->first();
        return $profile->delete();
    }
}