<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>JLB - Admin</title>
    <link rel="icon" href="img/JLB.jpg" type="image/png">
    <link href="css/account.css" rel="stylesheet" type="text/css">
    <link href="css/font.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/print.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            // document.getElementsByClassName("wrapper")[0].style.display = "inline";
            showGroup("transactions");
            // showGroup("build");
        };

        function showCat(cat) {
            var o = document.getElementsByClassName("category");
            for (var i = 0; i < o.length; i++) {
                if (cat == o[i].id) {
                    o[i].className = "category";
                } else {
                    o[i].className = "category hide";
                }
            }
        }

        function showGroup(nav) {
            var o = document.getElementsByClassName("group");
            for (var i = 0; i < o.length; i++) {
                if (nav == o[i].id || (nav == "products" && o[i].id == "add-prod")) {
                    o[i].style.display = "inline-block";
                    if (nav == "edit-prod" || nav == "remove-prod" || nav == "edit-category") {
                        document.getElementById("products").style.display = "inline-block";
                    }
                    if (nav == "edit-build" || nav == "add-build") {
                        document.getElementById("build").style.display = "inline-block";
                    }
                } else {
                    o[i].style.display = "none";
                }
            }
        }

        function editCategory(){
            showGroup("edit-category");
        }

        var catid = 1;
        function addCategory(){
            var list = document.getElementById("cat-list");
            list.innerHTML = list.innerHTML + '<span class="cat" id="newcat-'+catid+'">' +
                        '<i class="fa fa-minus-circle" onclick=\'document.getElementById("newcat-'+catid+'").remove()\'></i>' +
                        ' <input type="text" placeholder="Category" name="category" value="">' +
                    '</span>'
            catid++;
        }

        function editItem(itemid) {
            showGroup("edit-prod");
            document.getElementById("edit_id").value = itemid;

            var category = document.getElementById("edit_category");
            var name = document.getElementById("edit_name");
            var prize = document.getElementById("edit_prize");
            var stock = document.getElementById("edit_stock");
            var description = document.getElementById("edit_description");

            $.get('product/' + itemid,
                    function (data) {
                        category.value = data["product"]["category"];
                        prize.value = data["product"]["prize"];
                        name.value = data["product"]["name"];
                        stock.value = data["product"]["stock"];
                        description.innerHTML = data["product"]["description"];
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }

        function removeItem(itemid) {
            showGroup("remove-prod");
            document.getElementById("delete_id").value = itemid;

            var category = document.getElementById("delete_category");
            var name = document.getElementById("delete_name");
            var prize = document.getElementById("delete_prize");
            var stock = document.getElementById("delete_stock");
            var description = document.getElementById("delete_description");

            $.get('product/' + itemid,
                    function (data) {
                        category.value = data["product"]["category"];
                        prize.value = data["product"]["prize"];
                        name.value = data["product"]["name"];
                        stock.value = data["product"]["stock"];
                        description.innerHTML = data["product"]["description"];
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }

        function editGroupItem(itemid) {
            showGroup("edit-build");

            document.getElementById("custom_id").value = itemid;
            $prize = document.getElementById("custom_prize");
            $enable = document.getElementById("custom_enable");


            $.get('custom/' + itemid,
                    function (data) {
                        var custom = data["custom"];
                        $prize.value = custom["prize"];
                        $enable.value = custom["enable"]
                       console.log(custom);
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });


        }

        function addGroupItem(itemcat) {
            document.getElementById("addBuildCat").value = itemcat;
            showGroup("add-build");
        }

        function buttonEnable(id) {
            document.getElementById(id).disabled = false;
        }
        function buttonDisable(id) {
            document.getElementById(id).disabled = true;
        }

        function changeDate() {
            var reservationTable = document.getElementById("reservationTable").getElementsByTagName('tbody')[0];
            var date = document.getElementById("rdate");
            reservationTable.innerHTML = "";
            // console.log("date value : " + date.value);
            $.get('reservation?date=' + date.value,
                    function (data) {
                        var reservations = data["reservations"];
                        for (var i = 0; i < reservations.length; i++) {
                            var obj = reservations[i];
                            // console.log(obj);
                            var row = reservationTable.insertRow(i);
                            var c1 = row.insertCell(0);
                            var c2 = row.insertCell(1);
                            var c3 = row.insertCell(2);
                            c1.innerHTML = obj.user.email;
                            c2.innerHTML = obj.starttime;
                            c3.innerHTML = obj.endtime;
                            row.setAttribute('onclick', 'showGroup("trans-reserve")');
                            // console.log(row);
                        }
                        if (reservations.length == 0) {
                            var row = reservationTable.insertRow(0);
                            var c1 = row.insertCell(0);
                            var c2 = row.insertCell(1);
                            var c3 = row.insertCell(2);
                            c1.innerHTML = "N/A";
                            c2.innerHTML = "-";
                            c3.innerHTML = "-";
                        }
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }

        function getOrder(ordernum) {
            var order_date = document.getElementById("order_date");
            var order_email = document.getElementById("order_email");
            var order_status = document.getElementById("order_status");
            var order_body = document.getElementById("orderTable").getElementsByTagName('tbody')[0];
            var num = document.getElementById("trans-o-num");
            order_body.innerHTML = "";
            console.log("ordernum value : " + ordernum);
            $.get('order?ordernum=' + ordernum,
                    function (data) {
                        var orders = data["orders"];
                        for (var i = 0; i < orders.length; i++) {
                            var obj = orders[i];
                            console.log(obj);
                            num.innerHTML = obj.ordernum;
                            order_date.value = obj.created_at;
                            order_email.value = obj.user.email;
                            order_status.value = obj.status;
                            var row = order_body.insertRow(i);
                            var c1 = row.insertCell(0);
                            var c2 = row.insertCell(1);
                            var c3 = row.insertCell(2);
                            c1.innerHTML = obj.product.name;
                            c2.innerHTML = obj.product.category;
                            c3.innerHTML = "₱" + obj.product.prize;
                            order_email.dataset.userid = obj.user.id;
                            num.dataset.cash = obj.change;
                            num.dataset.amount = obj.product.prize;
                        }
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }

        function getOrderAll() {
            var tTable = document.getElementById("transactionTable").getElementsByTagName('tbody')[0];
            tTable.innerHTML = "";
            $.get('order',
                    function (data) {
                        var orders = data["orders"];
                        for (var i = 0; i < orders.length; i++) {
                            var obj = orders[i];
                            console.log(obj);
                            var row = tTable.insertRow(i);
                            row.onclick = (function (e) {
                                e = e || window.event;
                                var target = e.srcElement || e.target;
                                while (target && target.nodeName !== "TR") {
                                    target = target.parentNode;
                                }
                                if (target) {
                                    var cells = target.getElementsByTagName("td");
                                }
                                showGroup("trans-order");
                                getOrder(cells[1].innerHTML);
                            });
                            var c1 = row.insertCell(0);
                            var c2 = row.insertCell(1);
                            var c3 = row.insertCell(2);
                            var c4 = row.insertCell(3);
                            c1.innerHTML = obj.created_at;
                            c2.innerHTML = obj.ordernum;
                            c3.innerHTML = obj.user.email;
                            c4.innerHTML = obj.status;
                        }
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }

        function updateStatus() {
            var num = document.getElementById("trans-o-num");
            var order_status = document.getElementById("order_status");
            $.ajax({
                url: 'order/' + num.innerHTML,
                type: 'PUT',
                data: "status=" + order_status.value,
                success: function (data) {
                    var status = data["status"];
                    console.log(status);
                    if (status === 1) {
                        alert("Order Status has been updated!");
                    }
                    getOrderAll();
                }
            });
        }

        function printPage(page) {
            document.getElementById("OR").style.display = "none";
            document.getElementById("Report").style.display = "none";
            document.getElementById("Report").style.display = "none";
            document.getElementById("Report-reserve").style.display = "none";
            document.getElementById("Guitar").style.display = "none";
            document.getElementById(page).style.display = "block";
            if (page == "OR") {
                document.getElementById("print-ordernum").innerHTML = document.getElementById("trans-o-num").innerHTML;
                document.getElementById("print-userid").innerHTML = document.getElementById("order_email").dataset.userid;
                document.getElementById("print-email").innerHTML = document.getElementById("order_email").value;
                document.getElementById("print-order-date").innerHTML = document.getElementById("order_date").value;
                document.getElementById("print-or-t").innerHTML = document.getElementById("orderTable").innerHTML;
                var cash = document.getElementById("print-cash");
                cash.innerHTML = document.getElementById("trans-o-num").dataset.cash;
                var amount = document.getElementById("print-amount");
                amount.innerHTML = document.getElementById("trans-o-num").dataset.amount;
                var change = document.getElementById("print-change");
                change.innerHTML = parseInt(cash.innerHTML.split(".")[0]) - parseInt(amount.innerHTML.split(".")[0]);
            } else if (page == "Report") {
                document.getElementById("print-tbl-t").innerHTML = document.getElementById("transactionTable").innerHTML;
            } else if (page == "Report-reserve") {
                document.getElementById("print-rdate").innerHTML = document.getElementById("rdate").value;
                document.getElementById("print-tbl-r").innerHTML = document.getElementById("reservationTable").innerHTML;
            } else if (page == "Guitar") {
                var src = document.getElementById("template");
                // var dst = document.getElementById("destCanvas");
                // var dstctx = dst.getContext('2d');
                // dstctx.drawImage(src,0,0);
                document.getElementById("cnvsImg").src=src.toDataURL("image/png");
            }
            window.print();
        }

        function removeUserConfirm(id) {
            if (confirm('Are you sure you want to delete this user into the database?')) {
                $.ajax({
                    url: 'profile/' + id,
                    type: 'DELETE',
                    success: function (data) {
                        console.log(data);
                        location.reload();
                    }
                });
            }
        }

        function deleteProduct() {
            var id = document.getElementById("delete_id").value;
            $.ajax({
                url: 'product/' + id,
                type: 'DELETE',
                success: function (data) {
                    console.log(data);
                    location.reload();
                }
            });

        }
    </script>
</head>

<body>

<div class="wrapper">

    <nav id="navbar" class="navbar-collapse">
        <div class="container">

            <div id="logo-container">
                <span id="logo"></span>
                <a id="logo-link" href="/">JLB Woodart</a>
            </div>

            <div id="user" onclick='showGroup("account");'>
                <span id="pic"></span>
                <div class="user-detail">
                    <h3>Admin</h3>
                    <i class="fa fa-refresh"></i><span>Update</span>
                </div>
            </div>

            <div class="option" onclick='showGroup("transactions");'>
                <i class="fa fa-table"></i>
                <span>Transactions</span>
            </div>
            <div class="option" onclick='showGroup("products");'>
                <i class="fa fa-book"></i>
                <span>Products</span>
            </div>
            <!--<div class="option" onclick='showGroup("reservations"); changeDate();'>
                <i class="fa fa-calendar"></i>
                <span>Reservations</span> -->
            </div>
            <div class="option" onclick='location.replace("http://customize.esy.es/local/");'>
                <i class="fa fa-puzzle-piece"></i>
                <span>Build</span>
            </div>
            <div class="option" onclick='showGroup("users");'>
                <i class="fa fa-user"></i>
                <span>Users</span>
            </div>

            <a href="{{ url('/logout') }}">
                <div id="nav-fabar">
                    <i class="fa fa-sign-out"></i>
                </div>
            </a>

        </div>
    </nav>

    <div id="content">
        <div id="transactions" class="group trans">
            <h1>
                <i class="fa fa-table"></i> Transactions
                <span class="printBtn" onclick='printPage("Report")'>
                    <i class="fa fa-print"></i>
                </span>
            </h1>
            <div class="trans-content">
                <input type="date" value="<?php echo date('Y-m-d'); ?>">
                <table id="transactionTable" class="trans-tbl-main">
                    <thead>
                    <td>Transaction Date</td>
                    <td>Transaction #</td>
                    <td>Customer</td>
                    <td>Status</td>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr onclick='showGroup("trans-order"); getOrder({{$order->ordernum}});'>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->ordernum}}</td>
                            <td>
                                    {{$order->userid}}
                            </td>
                            <td>{{$order->status}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div id="trans-order" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Order #<span id="trans-o-num" data-cash="" data-amount="">123</span>
                <span class="trans-closebtn" onclick='showGroup("transactions")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Customer</h2>
                <input id="order_email" data-userid="" type="email" readonly="">
                <h2>Order Date</h2>
                <input id="order_date" type="text" readonly="">
                <h2>Order Status</h2>
                <select id="order_status" onchange='buttonEnable("trans-o-savebtn")'>
                    <option value="Ongoing">Ongoing</option>
                    <option value="Delivered">Delivered</option>
                    <option value="Delivering">Delivering</option>
                    <option value="Canceled">Canceled</option>
                </select>
                <h2>Estimated Completion Date</h2>
                <input id="order_est_complettion_date" type="date">
                <table id="orderTable">
                    <thead>
                    <td>Product</td>
                    <td>Category</td>
                    <td>Price</td>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <div class="add-btn">
                    <button id="trans-o-savebtn" onclick='buttonDisable(this.id);  updateStatus();' disabled="">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                    <button type="button" onclick='printPage("OR")'><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>

        <div id="trans-reserve" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Reservation #<span id="trans-r-num">123</span>
                <span class="trans-closebtn" onclick='showGroup("reservations")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Customer</h2>
                <input type="email" value="user@gmail.com">
                <h2>Reservation Date</h2>
                <input type="date" value="2016-09-03" readonly="">
                <table>
                    <thead>
                    <td>Start Time</td>
                    <td>End Time</td>
                    </thead>
                    <tr>
                        <td>00:00</td>
                        <td>00:00</td>
                    </tr>
                </table><!-- 
                <div class="add-btn">
                    <button type="button" onclick='printPage("OR-reserve")'><i class="fa fa-print"></i> Print</button>
                </div> -->
            </div>
        </div>

        <div id="trans-custom" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Customization Order #<span id="trans-c-num">123</span>
                <span class="trans-closebtn" onclick='showGroup("transactions")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Customer</h2>
                <input type="email" value="user@gmail.com" readonly="">
                <h2>Order Date</h2>
                <input type="date" value="2016-09-03" readonly="">
                <h2>Order Status</h2>
                <select onchange='buttonEnable("trans-c-savebtn")'>
                    <option>Complete</option>
                    <option>In Progress</option>
                    <option>Canceled</option>
                </select>
                <div class="add-btn">
                    <button id="trans-c-savebtn" onclick='buttonDisable(this.id)' disabled=""><i
                                class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </div>
        </div>

        <div id="account" class="group">
            <h1><i class="fa fa-user"></i> Account Info</h1>
            <div class="acc-content">
                <h2>Email Address</h2>
                <input type="email" value="user@gmail.com" onchange='buttonEnable("acc-updatebtn")'>
                <h2>Password</h2>
                <input type="password" value="password" onchange='buttonEnable("acc-updatebtn")'>
                <h2>Confirm Password</h2>
                <input type="password" value="password" onchange='buttonEnable("acc-updatebtn")'>
                <div class="add-btn">
                    <button onclick='showGroup("transactions")'><i class="fa fa-times"></i> Cancel</button>
                    <button id="acc-updatebtn" onclick='buttonDisable(this.id)' disabled=""><i class="fa fa-check"></i>
                        Update
                    </button>
                </div>
            </div>
        </div>

        <div id="products" class="group">
            <h1><i class="fa fa-book"></i> Products</h1>

            <div id="categories">
                <div class="option" data-itemcategory="cat1" onclick="showCat(this.dataset.itemcategory);">Beds</div>
                <div class="option" data-itemcategory="cat2" onclick="showCat(this.dataset.itemcategory);">Chairs</div>
                <div class="option" onclick="editCategory()" style="width: 30px"><i class="fa fa-pencil-square-o"></i></div>
            </div>

            <div id="cat1" class="category">
                @foreach($guitars as $guitar)
                    <div class="show-prod">
                        <img id="p{{$guitar->id}}-img" src="{{$guitar->pic}}">
                        <h2 id="p{{$guitar->id}}-nm">{{$guitar->name}} <span class="stock">(<b
                                        id="p{{$guitar->id}}-stock">{{$guitar->stock}}</b> in Stock)</span></h2>
                        <h3>₱ <span id="p{{$guitar->id}}-prc">{{$guitar->prize}}</span></h3>
                        <p id="p{{$guitar->id}}-desc">
                            {{$guitar->description}}
                        </p>
                        <div class="add-btn">
                            <button data-id="{{$guitar->id}}" onclick='editItem(this.dataset.id)'>
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </button>
                            <button data-id="{{$guitar->id}}" onclick='removeItem(this.dataset.id)'>
                                <i class="fa fa-trash"></i> Remove
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>


            <div id="cat2" class="category hide">
                @foreach($headsets as $headset)
                    <div class="show-prod">
                        <img id="p{{$headset->id}}-img" src="{{$headset->pic}}">
                        <h2 id="p{{$headset->id}}-nm">{{$headset->name}} <span class="stock">(<b
                                        id="p{{$guitar->id}}-stock">10</b> in Stock)</span></h2>
                        <h3>₱ <span id="p{{$headset->id}}-prc">{{$headset->prize}}</span></h3>
                        <p id="p{{$headset->pic}}-desc">
                            {{$headset->description}}
                        </p>
                        <div class="add-btn">
                            <button data-id="{{$guitar->id}}" onclick='editItem(this.dataset.id)'>
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </button>
                            <button data-id="{{$guitar->id}}" onclick='removeItem(this.dataset.id)'>
                                <i class="fa fa-trash"></i> Remove
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div id="add-prod" class="group">
            <h1><i class="fa fa-plus-circle"></i> Add Product</h1>
            <div class="container">
                <form role="form" enctype="multipart/form-data" method="POST" action="/product">
                    <select name="category">
                        <option value="Guitar">Beds</option>
                        <option value="Headset">Chairs</option>
                    </select>
                    <input type="text" placeholder="Name" name="name"/>
                    <input type="file" name="pic"/>
                    <input type="number" name="prize" placeholder="Price"/>
                    <input type="number" name="stock" placeholder="Stocks"/>
                    <textarea rows="4" cols="50" name="description" placeholder="Description"></textarea>
                    <div class="add-btn">
                        <button type="submit"><i class="fa fa-plus"></i> Add</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="edit-prod" class="group">
            <h1><i class="fa fa-pencil-square-o"></i> Edit Product</h1>
            <div class="container">
                <form role="form" enctype="multipart/form-data" method="POST" action="/product/update">
                    <select id="edit_category" name="edit_category">
                        <option value="Guitar">Beds</option>
                        <option value="Headset">Chairs</option>
                    </select>
                    <input type="text" id="edit_name" placeholder="Name" name="edit_name"/>
                    <input type="file" id="edit_pic" name="edit_pic"/>
                    <input type="number" id="edit_prize" name="edit_prize" placeholder="Price"/>
                    <input type="number" id="edit_stock" name="stock" placeholder="Stocks"/>
                    <input type="hidden" id="edit_id" name="edit_id">
                    <textarea id="edit_description" rows="4" cols="50" name="description" placeholder="Description"></textarea>
                    <div class="add-btn">
                        <button type="button" onclick='showGroup("products")'><i class="fa fa-ban"></i> Cancel</button>
                        <button type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="remove-prod" class="group">
            <h1><i class="fa fa-pencil-square-o"></i> Remove Product</h1>
            <div class="container">
                <input type="text" id="delete_category" placeholder="Category" name="name" readonly=""/>
                <input type="text" id="delete_name" placeholder="Name" name="name" readonly=""/>
                <input type="number" id="delete_prize" name="prize" placeholder="Price" readonly=""/>
                <input type="number" id="delete_stock" name="stock" placeholder="Stocks" readonly=""/>
                <input type="hidden" id="delete_id">
                <textarea id="delete_description" rows="4" cols="50" name="description" placeholder="Description" readonly=""></textarea>
                <div class="add-btn">
                    <button type="button" onclick='showGroup("products")'><i class="fa fa-times"></i> No</button>
                    <button type="button" onclick="deleteProduct()"><i class="fa fa-check"></i> Yes</button>
                </div>
            </div>
        </div>

        <div id="edit-category" class="group">
            <h1><i class="fa fa-pencil-square-o"></i> Edit Categories</h1>
            <div class="container">
                <div id="cat-list"> 
                    <span class="cat" id="cat-1">
                        <i class="fa fa-minus-circle" onclick='document.getElementById("cat-1").remove()'></i>
                        <input type="text" placeholder="Category" name="category" value="Guitars"/>
                    </span>
                    <span class="cat" id="cat-2">
                        <i class="fa fa-minus-circle" onclick='document.getElementById("cat-2").remove()'></i>
                        <input type="text" placeholder="Category" name="category" value="Headsets"/>
                    </span>
                </div>
                <span class="cat" id="add">
                    <i class="fa fa-plus-circle" onclick='addCategory()'></i>
                    <input type="text" value="Add new category" readonly="" disabled="" />
                </span>
                <div class="add-btn">
                        <button type="button" onclick='showGroup("products")'><i class="fa fa-ban"></i> Cancel</button>
                        <button type="button"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
            </div>
        </div>

        <div id="reservations" class="group">
            <h1>
                <i class="fa fa-calendar"></i> Reservations
                <span class="printBtn" onclick='printPage("Report-reserve")'>
                    <i class="fa fa-print"></i>
                </span>
            </h1>
            <div class="container">
                <input id="rdate" type="date" value="<?php echo date('Y-m-d'); ?>" onchange="changeDate();"></input>
                <table id="reservationTable">
                    <thead>
                    <td>Email</td>
                    <td>Start Time</td>
                    <td>End Time</td>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

        <div id="users" class="group">
            <h1><i class="fa fa-table"></i> User List</h1>
            <div class="users-content">
                <table>
                    <thead>
                    <td>Email Address</td>
                    <td>Delivery Address</td>
                    <td>Contact Number</td>
                    </thead>
                    @foreach($profiles as $profile)
                        <tr>
                            <td><i class="fa fa-times-circle"
                                   onclick="removeUserConfirm({{$profile->userid}})"></i>{{$profile->user->email}}</td>
                            <td>{{$profile->address}}</td>
                            <td>{{$profile->contactnumber}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div id="build" class="group">
            <h1>
                <i class="fa fa-puzzle-piece"></i> Build
                <span class="printBtn" onclick='printPage("Guitar")'>
                    <i class="fa fa-print"></i>
                </span>
            </h1>
            <canvas id="template" width="836" height="300"></canvas>
            <script type="text/javascript">
                var tmp = document.getElementById("template");
                var ctx = tmp.getContext('2d');
                var images;
                var totprice = 0;

                function loadImages(sources, callback) {
                    images = {};
                    var loadImages = 0;
                    var numImages = 0;

                    for (var src in sources) {
                        numImages++;
                    }
                    for (var src in sources) {
                        images[src] = new Image();
                        images[src].onload = function () {
                            if (++loadImages >= numImages);
                            callback(images);
                        };
                        images[src].src = sources[src];
                    }
                }

                var sources = {
                    overlay: "/imgs/customs/Overlay.png",
                    overlay_paddle: "/imgs/customs/Overlay_Paddle.png",
                    overlay_satin: "/imgs/customs/Overlay_Satin.png",
                    Default: "/imgs/customs/Natural.png",
                    Black: "/imgs/customs/Black.png",
                    Red: "/imgs/customs/Red.png",
                    Orange: "/imgs/customs/Orange.png",
                    Green: "/imgs/customs/Green.png",
                    SonicBlue: "/imgs/customs/SonicBlue.png",
                    headstock_Default: "/imgs/customs/Headstock_Natural.png",
                    headstock_Black: "/imgs/customs/Headstock_Black.png",
                    headstock_Red: "/imgs/customs/Headstock_Red.png",
                    headstock_Orange: "/imgs/customs/Headstock_Orange.png",
                    headstock_Green: "/imgs/customs/Headstock_Green.png",
                    headstock_SonicBlue: "/imgs/customs/Headstock_SonicBlue.png",
                    stopTail: "/imgs/customs/Stop_Tail.png",
                    mountingRings: "/imgs/customs/Mounting_Rings.png",
                    knobs: "/imgs/customs/Knobs.png",
                    switch: "/imgs/customs/Switch.png",
                    tuners: "/imgs/customs/Tuners.png",
                    fretboard_Default: "/imgs/customs/Fretboard_Rosewood.png",
                    fretboard_Maple: "/imgs/customs/Fretboard_Maple.png",
                    fretboard_Rosewood: "/imgs/customs/Fretboard_Rosewood.png",
                    fretboard_Rosewood_blocks: "/imgs/customs/Fretboard_Rosewood_Blocks.png",
                    pickguard_Default: "/imgs/customs/None.png",
                    pickguard_White: "/imgs/customs/Pickguard_White.png",
                    pickguard_Black: "/imgs/customs/Pickguard_Black.png",
                    pickguard_Red: "/imgs/customs/Pickguard_Red.png",
                    pickguard_Blue: "/imgs/customs/Pickguard_Blue.png",
                    pickguard_Yellow: "/imgs/customs/Pickguard_Yellow.png",
                    bridge_Default: "/imgs/customs/Bridge_Chrome.png",
                    bridge_Chrome: "/imgs/customs/Bridge_Chrome.png",
                    bridge_Gold: "/imgs/customs/Bridge_Gold.png",
                    neck_Default: "/imgs/customs/Neck_Chrome.png",
                    neck_Chrome: "/imgs/customs/Neck_Chrome.png",
                    neck_Gold: "/imgs/customs/Neck_Gold.png"
                }

                loadImages(sources, function (images) {
                    ctx.drawImage(images.Default, 0, 4);
                    ctx.drawImage(images.headstock_Default, 692, 94);
                    ctx.drawImage(images.stopTail, 108, 97);
                    ctx.drawImage(images.mountingRings, 171, 100);
                    ctx.drawImage(images.bridge_Default, 170, 110);
                    ctx.drawImage(images.neck_Default, 250, 110);
                    ctx.drawImage(images.fretboard_Default, 296, 114);
                    ctx.drawImage(images.overlay, 0, 0);
                    ctx.drawImage(images.overlay_paddle, 797, 90);
                    ctx.drawImage(images.pickguard_Default, 167, 165);
                    ctx.drawImage(images.knobs, 80, 195);
                    ctx.drawImage(images.switch, 103, 198);
                    ctx.drawImage(images.tuners, 719, 88);
                    ctx.drawImage(images.overlay_satin, -10, -10);
                    ctx.fillStyle = "red";
                    ctx.font = "20px Oswald, sans-serif";
                    totprice = parseInt(document.getElementById("srvcChrg").value);
                    ctx.fillText("₱" + totprice + ".00", 750, 20);
                });

                function reDraw() {
                    var c1 = document.querySelector('input[name="body"]:checked');
                    var c2 = document.querySelector('input[name="pickguard"]:checked');
                    var c3 = document.querySelector('input[name="fretboard"]:checked');
                    var c4 = document.querySelector('input[name="bridge"]:checked');
                    var baseprice = parseInt(document.getElementById("srvcChrg").value);
                    totprice = baseprice + parseInt(c1.dataset.price) + parseInt(c2.dataset.price) + parseInt(c3.dataset.price) + parseInt(c4.dataset.price)
                    ctx.drawImage(images[c1.value], 0, 4);
                    ctx.drawImage(images["headstock_" + c1.value], 692, 94);
                    ctx.drawImage(images.stopTail, 108, 97);
                    ctx.drawImage(images.mountingRings, 171, 100);
                    ctx.drawImage(images["bridge_" + c4.value], 170, 110);
                    ctx.drawImage(images["neck_" + c4.value], 250, 110);
                    ctx.drawImage(images["fretboard_" + c3.value], 296, 114);
                    ctx.drawImage(images.overlay, 0, 0);
                    ctx.drawImage(images.overlay_paddle, 797, 90);
                    ctx.drawImage(images["pickguard_" + c2.value], 167, 165);
                    ctx.drawImage(images.knobs, 80, 195);
                    ctx.drawImage(images.switch, 103, 198);
                    ctx.drawImage(images.tuners, 719, 88);
                    ctx.drawImage(images.overlay_satin, -10, -10);
                    ctx.font = "20px Oswald, sans-serif";
                    ctx.fillText("₱" + totprice + ".00", 750, 20);
                }
            </script>
            <div class="custom-selection">
                <div class="custom-group">
                    <h2>Body</h2>
                    <ul>
                        @foreach($bodies as $body)
                            <li>
                                <input onchange="reDraw()" type="radio" name="body" id="body{{$body->id}}"
                                       value="{{$body->name}}" data-price="{{$body->prize}}"
                                        @if ($body->name == "Default")
                                            checked
                                        @endif>
                                <label for="body{{$body->id}}">
                                    <span class="preview" style='background-image: url("{{$body->imgs}}")'></span>
                                    {{$body->name}} <b>₱{{$body->prize}}</b>
                                </label>
                                <i class="fa fa-pencil-square" data-id="{{$body->id}}" onclick="editGroupItem(this.dataset.id)"></i>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Pickguard</h2>
                    <ul>
                        @foreach($pickguards as $pickguard)
                            <li>
                                <input onchange="reDraw()" type="radio" name="pickguard"
                                       id="pickguard{{$pickguard->id}}" value="{{$pickguard->name}}"
                                       data-price="{{$pickguard->prize}}"
                                        @if ($pickguard->name == "Default")
                                            checked
                                        @endif>
                                <label for="pickguard{{$pickguard->id}}">
                                    <span class="preview" style='background-image: url("{{$pickguard->imgs}}")'></span>
                                    {{$pickguard->name}} <b>₱{{$pickguard->prize}}</b>
                                </label>
                                <i class="fa fa-pencil-square" data-id="{{$pickguard->id}}" onclick="editGroupItem(this.dataset.id)"></i>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Fretboard</h2>
                    <ul>
                        @foreach($fretboards as $fretboard)
                            <li>
                                <input onchange="reDraw()" type="radio" name="fretboard"
                                       id="fretboard{{$fretboard->id}}" value="{{$fretboard->name}}"
                                       data-price="{{$fretboard->prize}}"
                                        @if ($fretboard->name == "Default")
                                            checked
                                        @endif>
                                <label for="fretboard{{$fretboard->id}}">
                                    <span class="preview" style='background-image: url("{{$fretboard->imgs}}")'></span>
                                    {{$fretboard->name}} <b>₱{{$fretboard->prize}}</b>
                                </label>
                                <i class="fa fa-pencil-square" data-id="{{$fretboard->id}}" onclick="editGroupItem(this.dataset.id)"></i>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Bridge</h2>
                    <ul>
                        @foreach($bridges as $bridge)
                            <li>
                                <input onchange="reDraw()" type="radio" name="bridge" id="bridge{{$bridge->id}}"
                                       value="{{$bridge->name}}" data-price="{{$bridge->prize}}"
                                        @if ($bridge->name == "Default")
                                            checked
                                        @endif>
                                <label for="bridge{{$bridge->id}}">
                                    <span class="preview" style='background-image: url("{{$bridge->imgs}}")'></span>
                                    {{$bridge->name}} <b>₱{{$bridge->prize}}</b>
                                </label>
                                <i class="fa fa-pencil-square" data-id="{{$bridge->id}}" onclick="editGroupItem(this.dataset.id)"></i>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <p class="note">Inclusive of ₱<input id="srvcChrg" type="number" value="250" onchange="reDraw();//+submit">
                service charge.</p>
        </div>

        <div id="edit-build" class="group">
            <h1><i class="fa fa-pencil-square-o"></i> Edit Item</h1>
            <div class="container">
                <form role="form" enctype="multipart/form-data" method="POST" action="custom">
                    <select name="enable" id="custom_enable">
                        <option value="1">enable</option>
                        <option value="0">disable</option>
                    </select>
                    <input type="number" name="prize" placeholder="Price" id="custom_prize"/>
                    <input type="hidden" name="id" id="custom_id">
                    <div class="add-btn">
                        <button type="button" onclick='showGroup("build")'><i class="fa fa-ban"></i> Cancel</button>
                        <button type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="add-build" class="group">
            <h1><i class="fa fa-pencil-square-o"></i> Add Item</h1>
            <div class="container">
                <form role="form" enctype="multipart/form-data" method="POST" action="">
                    <select id="addBuildCat" name="category">
                        <option value="Body">Body</option>
                        <option value="Pickguard">Pickguard</option>
                        <option value="Fretboard">Fretboard</option>
                        <option value="Bridge">Bridge</option>
                    </select>
                    <input type="text" placeholder="Name" name="name"/>
                    <input type="file" name="pic"/>
                    <input type="number" name="prize" placeholder="Price"/>
                    <div class="add-btn">
                        <button type="button" onclick='showGroup("build")'><i class="fa fa-ban"></i> Cancel</button>
                        <button type="button"><i class="fa fa-plus"></i> Add</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div id="print">
    <div id="OR" class="OR">
        JLBWoodartFurniture<br>
        C-624 B Quirino Highway Bag-Bag Novaliches Quezon City, Philippines<br>
        +639234543684<br>
        <br>
        <span id="print-order-date">00/00/0000</span><br>
        Order Number<br>
        <span id="print-ordernum">00000000</span>
        <hr>
        Customer Information<br>
        <div class="align-left">ID: <span id="print-userid">00000000</span><br>
            Email: <span id="print-email">user@gmail.com</span>
            <hr>
            <table id="print-or-t"><!--
            <tr>
                <td>product1</td>
                <td>0.00</td>
            </tr>
            <tr>
                <td>product2</td>
                <td>0.00</td>
            </tr> -->
            </table>
            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Amount Due</td>
                    <td>P<span id="print-amount">0</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cash</td>
                    <td>P<span id="print-cash">0</span></td>
                </tr>
                <tr>
                    <td>Change</td>
                    <td>P<span id="print-change">0</span>.00</td>
                </tr>
            </table>
            <hr>
        </div>
        This serves as your official Receipt.
        Thank you, and please come again.
    </div><!-- 
    <div id="OR-reserve" class="OR">
        KnoxVille<br>
        654 Quirino Highway, Bagbag, Novaliches, Quezon City, Philippines<br>
        0917-851-7727<br>
        <br>
        00/00/0000<br>
        Order Number<br>
        00000000
        <hr>
        Customer Information<br>
        <div class="align-left">ID: 00000000<br>
        Email: user@gmail.com
        <hr>
        <table id="print-or-r">
            <tr>
                <td>Start Time</td>
                <td>End Time</td>
            </tr>
            <tr>
                <td>00:00</td>
                <td>00:00</td>
            </tr>
        </table>
        <table>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Amount Due</td>
                <td>P0.00</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Cash</td>
                <td>P0.00</td>
            </tr>
            <tr>
                <td>Change</td>
                <td>P0.00</td>
            </tr>
        </table>
        <hr>
        </div>
        This serves as your official Receipt.
        Thank you, and please come again.
    </div> -->
    <div id="Report" class="Report">
        <div class="align-center">
            JLBWoodartFurniture<br>
            C-624 B Quirino Highway Bag-Bag Novaliches Quezon City, Philippines<br>
            +639234543684
            <h1>JLB Transactions Report</h1>
        </div>
        <hr>
        <table id="print-tbl-t"></table>
    </div>
    <div id="Report-reserve" class="Report">
        <div class="align-center">
            JLBWoodartFurniture<br>
            C-624 B Quirino Highway Bag-Bag Novaliches Quezon City, Philippines<br>
            +639234543684<br>
            <h1>JLB Reservations Report</h1>
            For: <span id="print-rdate">00/00/0000<span>
        </div>
        <hr>
        <table id="print-tbl-r"></table>
    </div>
    <div id="Guitar">
        <img id="cnvsImg">
        <!-- <canvas id="destCanvas" width="836" height="300"></canvas> -->
    </div>
</div>

</body>

</html>