
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>JLB | Login</title>
    <link rel="icon" href="img/pro.jpg" type="image/png">
    <link href="css/form.css" rel="stylesheet" type="text/css">
    <link href="css/font.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="container">

    <div class="head">
        <span id="logo"></span>
        <span id="logo-link">JLBWoodart</span>
    </div>

    <form class="form" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h2><i class="fa fa-sign-in "></i> Log in</h2>
        <input type="email" placeholder="Email Address" name="email" value="" required="">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input type="password" placeholder="Password" name="password" required="">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <button type="submit">Login</button>
    </form>

    <div class="foot">
        <a href="/register" class="link">click here to create an account</a>
    </div>
</div>

</body>

</html>

