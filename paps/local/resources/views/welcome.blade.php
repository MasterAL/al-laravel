<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/jlb.JPG" type="image/jpg">
    <title>JLB | Home Page</title>

    <!--Google web fonts-->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,600,700,600italic,200,200italic' rel='stylesheet' type='text/css'>    
    <link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/animate.css">

    <!-- font awesome-->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/skillset.css">


    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css"> 
    <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">

    <link rel="stylesheet" type="text/css" href="style6.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      
      
    <![endif]-->
    <style type="text/css">
        * { box-sizing: border-box; }

body {
  padding: 0;
  margin: 0;
  text-align: center;
  font-family: 'Titillium Web', Helvetica, Arial, sans-serif;  
}


h1,h2,h3,h4,h5{
  color: #333;
}

a { text-decoration: none; }
a:hover{
  text-decoration: none;
  -webkit-transition: all .5s ease-in-out;
  -moz-transition: all .5s ease-in-out;
  -o-transition: all .5s ease-in-out;
  -ms-transition: all .5s ease-in-out;
  transition: all .5s ease-in-out;
}


.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color:blue;
    z-index: 99999;
}
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0,0,0,0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}



.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}

.modalDialog > div {
    width: 460px;
    position: relative;
    margin: 10% auto;
    padding: 5px 20px 13px 20px;
    border-radius: 10px;
}


.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}

.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: 13%;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    border-radius: 12px;
}
.close:hover { background: #00d9ff; }

.closes {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: 20%;
    text-align: center;
    top: -14px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    border-radius: 12px;
}
.closes:hover { background: #00d9ff; }
.link{
    position: absolute;
    right: 29%;
}
@keyframes spinner {
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(359deg);
  }
}
* {
  box-sizing: border-box;
}
.wrapper {
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  padding: 20px;
}
.login {
  border-radius: 2px 2px 5px 5px;
  padding: 10px 20px 20px 20px;
  width: 90%;
  max-width: 320px;
  background: #ffffff;
  position: relative;
  padding-bottom: 80px;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
}
.login.loading button {
  max-height: 100%;
  padding-top: 50px;
}
.login.loading button .spinner {
  opacity: 1;
  top: 40%;
}
.login.ok button {
  background-color: #8bc34a;
}
.login.ok button .spinner {
  border-radius: 0;
  border-top-color: transparent;
  border-right-color: transparent;
  height: 20px;
  animation: none;
  transform: rotateZ(-45deg);
}
.login input {
  display: block;
  padding: 15px 10px;
  margin-bottom: 10px;
  width: 100%;
  border: 1px solid #ddd;
  transition: border-width 0.2s ease;
  border-radius: 2px;
  color: #ccc;
}
.login input + i.fa {
  color: #fff;
  font-size: 1em;
  position: absolute;
  margin-top: -47px;
  opacity: 0;
  left: 0;
  transition: all 0.1s ease-in;
}
.login input:focus {
  outline: none;
  color: #444;
  border-color: #2196F3;
  border-left-width: 35px;
}
.login input:focus + i.fa {
  opacity: 1;
  left: 30px;
  transition: all 0.25s ease-out;
}
.login a {
  font-size: 0.8em;
  color: #2196F3;
  text-decoration: none;
}
.login .title {
  color: #444;
  font-size: 1.2em;
  font-weight: bold;
  margin: 10px 0 30px 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 20px;
}
.login button {
  width: 100%;
  height: 100%;
  padding: 10px 10px;
  background: #2196F3;
  color: #fff;
  display: block;
  border: none;
  margin-top: 20px;
  position: absolute;
  left: 0;
  bottom: 0;
  max-height: 60px;
  border: 0px solid rgba(0, 0, 0, 0.1);
  border-radius: 0 0 2px 2px;
  transform: rotateZ(0deg);
  transition: all 0.1s ease-out;
  border-bottom-width: 7px;
}
.login button .spinner {
  display: block;
  width: 40px;
  height: 40px;
  position: absolute;
  border: 4px solid #ffffff;
  border-top-color: rgba(255, 255, 255, 0.3);
  border-radius: 100%;
  left: 50%;
  top: 0;
  opacity: 0;
  margin-left: -20px;
  margin-top: -20px;
  animation: spinner 0.6s infinite linear;
  transition: top 0.3s 0.3s ease, opacity 0.3s 0.3s ease, border-radius 0.3s ease;
  box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.2);
}
.login:not(.loading) button:hover {
  box-shadow: 0px 1px 3px #2196F3;
}
.login:not(.loading) button:focus {
  border-bottom-width: 4px;
}

footer {
  display: block;
  padding-top: 50px;
  text-align: center;
  color: #ddd;
  font-weight: normal;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.2);
  font-size: 0.8em;
}
footer a, footer a:link {
  color: #fff;
  text-decoration: none;
}
/*==============Catalog=================*/
.modalDialog1 {
  position: fixed;
  font-family: Arial, Helvetica, sans-serif;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color:blue;
  z-index: 99999;
}
.modalDialog1 {
  position: fixed;
  font-family: Arial, Helvetica, sans-serif;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0,0,0,0.8);
  z-index: 99999;
  opacity:0;
  -webkit-transition: opacity 400ms ease-in;
  -moz-transition: opacity 400ms ease-in;
  transition: opacity 400ms ease-in;
  pointer-events: none;
}



.modalDialog1:target {
  opacity:1;
  pointer-events: auto;
}

.modalDialog1 > div {
  width: 500px;
  background-color: #3498db;
  position: relative;
  margin: 10% auto;
  padding: 5px 20px 13px 20px;
  border-radius: 10px;
}


.modalDialog1:target {
  opacity:1;
  pointer-events: auto;
}
.pic{
  border-radius: 12px;
  position: relative;
  right: 25%;
}
.brandname{
  position: absolute;
  right: 15%;
  top: 10px;
  font-weight: bolder;
  font-size: 20px;
  font-family: Cooper Black;  
  color: white;

}
.des{
  position: absolute;
  right: 35%;
  top: 35px;
  font-weight: bolder;
  color: white;

}
.desc{
  position: absolute;
  right: 25%;
  top: 35px;
  font-weight: bolder;
  color: white;

}
.pay{
  position: absolute;
  right: 5%;
  top: 230px;
  font-weight: bolder;
}
.add{
  border-radius: 12px;
  background-color: #1bf898;
}
.wrapper1 {
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  padding: 20px;
}
.close1 {
  background: #606061;
  color: #FFFFFF;
  line-height: 25px;
  position: absolute;
  right: 0%;
  text-align: center;
  top: -10px;
  width: 24px;
  text-decoration: none;
  font-weight: bold;
  border-radius: 12px;
}
.close1:hover { background: #00d9ff; }

/*============== Preloader ==============*/

/* Preloader */
#preloader {
  position: fixed;
  top:0;
  left:0;
  right:0;
  bottom:0;
  background-color:#fff; /* change if the mask should have another color then white */
  z-index:99; /* makes sure it stays on top */
}

#status {
  width:200px;
  height:200px;
  position:absolute;
  left:50%; /* centers the loading animation horizontally one the screen */
  top:50%; /* centers the loading animation vertically one the screen */
  background-image:url(img/preloader.gif); /* path to your loading animation */
  background-repeat:no-repeat;
  background-position:center;
  margin:-100px 0 0 -100px; /* is width and height divided by two */
}


/*============== Scroll up to  ==============*/

#toTop {
  display:none;
  text-decoration:none;
  position:fixed;
  bottom:10px;
  right:10px;
  overflow:hidden;
  width:51px;
  height:51px;
  border:none;
  text-indent:100%;
  background:url(img/ui.totop.png) no-repeat left top;
}

#toTopHover {
  background:url(img/ui.totop.png) no-repeat left -51px;
  width:51px;
  height:51px;
  display:block;
  overflow:hidden;
  float:left;
  opacity: 0;
  -moz-opacity: 0;
  filter:alpha(opacity=0);
}

#toTop:active, #toTop:focus {
  outline:none;
}






/*============== Scroll up to  ==============*/

.row {
  width: 100%;
  margin: 0 auto;
  position: relative;
  padding: 0 2%;
}

.content {
  max-width: 940px;
  width: 100%;
  margin: 0 auto;
  padding: 50px 2% 50px;
}

.main_header .row .content { padding: 0; }


.dark { 
  background: #333; 
}



/*
Fade content bs-carousel with hero headers
Code snippet by maridlcrmn (Follow me on Twitter @maridlcrmn) for Bootsnipp.com
Image credits: unsplash.com
*/

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
    height: 100vh;
}
.fade-carousel .carousel-inner .item {
    height: 100vh;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/

.hero {
    color: #fff;
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    color: #fff;
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.hero h1 span{
  color: #3498db;
}

.hero h3{
    color: #fff;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 2;
    background-color: #080d15;
    opacity: .7;
}

/********************************/
/*          Custom Buttons      */
/********************************/

.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #3498db;
    border-color: #3498db;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
  height: 100vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-1 {
  background-image: url(img/bed.jpg); 
}
.fade-carousel .slides .slide-2 {
  background-image: url(img/ClassicTable.jpg);
}
.fade-carousel .slides .slide-3 {
  background-image: url(img/photo-02.jpg);
}

/********************************/
/*          Media Queries       */
/********************************/
@media screen and (min-width: 980px){
    .hero { width: 980px; }    
}
@media screen and (max-width: 640px){
    .hero h1 { font-size: 4em; }    
}
.main_header {
  position: fixed;
  top: 0px;
  max-height: 70px;
  z-index: 999;
  width: 100%;
  background: none;
  overflow: hidden;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  opacity: 0;
  top: -100px;
  padding-bottom: 6px;
}


@media only screen and (min-width : 320px) {
        
}

/* Extra Small Devices, Phones */ 
@media only screen and (min-width : 480px) {

}


@media only screen and (max-width : 600px) {
  .cd-headline b{

 
  }
}





.open-nav { max-height: 400px !important; }

.open-nav .mobile-toggle {
  transform: rotate(-90deg);
  -webkit-transform: rotate(-90deg);
}

.sticky {
  background-color: rgba(255, 255, 255, 0.93);
  opacity: 1;
  top: 0px;
  border-bottom: 1px solid silver;
}

.logo {
  padding-top: 17px;    
  width: 50px;
  font-size: 25px;
  color: #3498db;
  text-transform: uppercase;
  float: left;
  display: block;
  margin-top: 0;
  line-height: 1;
  margin-bottom: 10px;
}

.main_menu li a:focus{
  outline: none;
}


.main_menu li a:active{
  text-decoration: none;
}

@media only screen and (max-width: 766px) {

.logo{
  padding-top: 0;
}

.main_header { padding-top: 25px; }

}



@media only screen and (max-width: 766px) {

.logo { float: none; }
}

nav {
  float: right;
  width: 60%;
}
@media only screen and (max-width: 766px) {

nav { width: 100%; }
}

nav ul {
  list-style: none;
  overflow: hidden;
  text-align: right;
  float: right;
}
@media only screen and (max-width: 766px) {

nav ul {
  padding-top: 10px;
  margin-bottom: 22px;
  float: left;
  text-align: center;
  width: 100%;
}
}

nav ul li {
  display: inline-block;
  margin-left: 11px;
  line-height: 1.5;
}

@media only screen and (max-width: 766px) {



nav ul li {
  width: 100%;
  padding: 7px 0;
  margin: 0;
}
}

nav ul a {
  color: #888888;
  text-transform: uppercase;
  font-size: 12px;
}

.mobile-toggle {
  display: none;
  cursor: Link Select;
  font-size: 20px;
  position: absolute;
  right: 22px;
  top: 0;
  width: 30px;
  -webkit-transition: all 200ms ease-in;
  -moz-transition: all 200ms ease-in;
  transition: all 200ms ease-in;
}
@media only screen and (max-width: 766px) {

.mobile-toggle { display: block; }
}




.mobile-toggle span {
  width: 30px;
  height: 4px;
  margin-bottom: 6px;
  border-radius: 1000px;
  background: #8f8f8f;
  display: block;
}

.mouse {
  display: block;
  margin: 0 auto;
  width: 26px;
  height: 46px;
  border-radius: 13px;
  border: 2px solid #ffffff;
  position: absolute;
  position: absolute;
  left: 51%;
  margin-left: -26px;
}

.mouse span {
  display: block;
  margin: 6px auto;
  width: 2px;
  height: 2px;
  border-radius: 4px;
  background: #ffffff;
  border: 1px solid transparent;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-name: scroll;
  animation-name: scroll;
}
 @-webkit-keyframes 
scroll {  0% {
 opacity: 1;
 -webkit-transform: translateY(0);
 transform: translateY(0);
}
 100% {
 opacity: 0;
 -webkit-transform: translateY(20px);
 transform: translateY(20px);
}
}
@keyframes 
scroll {  0% {
 opacity: 1;
 -webkit-transform: translateY(0);
 -ms-transform: translateY(0);
 transform: translateY(0);
}
 100% {
 opacity: 0;
 -webkit-transform: translateY(20px);
 -ms-transform: translateY(20px);
 transform: translateY(20px);
}
}


/*============== Service style ==============*/

.single_service{  
  margin-top: 100px;
  margin-bottom: 100px;
}
.single_service i{
  position: relative;
  overflow: hidden;
}

.single_service .ico{
  margin: 0 auto;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  font-size: 30px;
  color: #3498db;
  line-height: 76px;
  background: rgba(195, 185, 185, 0.21);
  width: 80px;
  height: 80px;
  text-align: center;
  border: solid 4px #fff;
  outline: 1px solid #dddddd;
  }

  .single_service:hover .ico{
  color:#F3F1F1;
  background: #3498db;  
  cursor:pointer;
  -webkit-transition:  ease-out .5s;
  -moz-transition:  ease-out .5s;
  -o-transition:  ease-out .5s;
  transition: ease-out .5s;  
  }

  .single_service .ico:hover{
  color:#F3F1F1;
  background: #3498db;  
  cursor:pointer;
  -webkit-transition:  ease-out .5s;
  -moz-transition:  ease-out .5s;
  -o-transition:  ease-out .5s;
  transition: ease-out .5s;  
  }


.single_service .ico i{
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

/* ===========work style===========*/

.work_area{
    background-color: #f8f8f8;
}


.work_section{
    margin-top: 60px;
    margin-bottom: 60px;
}

.work_section h1{
  text-transform: uppercase;
  font-weight: 600;
}

.work_section h1 span{
  color: #3498db;
}

.bs-example{
  margin-top: 30px;
  margin-bottom: 65px;
}

.bs-example .nav-tabs i{
  font-size: 50px;
  width: 80px;
  text-align: center;
}

@media only screen and (min-width : 765px) {

.bs-example .nav-tabs i{
  width: 57px;

 } 

}


@media (max-width: 764px){ 

.bs-example .nav-tabs i{
  font-size: 34px;
  width: 56px;
}

}

@media (max-width: 750px){ 

.bs-example .nav-tabs i{
  font-size: 20px;
  width: 15px;
}

}


.nav-tabs{
  border-bottom: none;
}


.tab-pane{
  border: 1px solid #ddd;
  padding: 42px;
  background-color:#ffffff; 
}


/* ===========getit style===========*/


.getit_area{
  background: #3498db;
}

.getit_area h1{
    color: #ffffff;
}

.getit_section{
    margin-top: 40px;
  margin-bottom: 40px;
}





/* ===========Portfolio style===========*/

.portfolio_area{
  margin-top: 40px;
  margin-bottom: 40px;
}

.portfolio_section{

}

.portfolio_section h1{
  color: #3498db;
  text-transform: uppercase;
  font-weight: 600;
}

.portfolio_top{

}

.portfolio{

}



#info {
  -webkit-border-radius:5px;
  -moz-border-radius:5px;
  border-radius:5px;        
  background:#fcf8e3;
  border:1px solid  #fbeed5;
  width:95%;
  max-width:900px;
  margin:0 auto 40px auto;
  font-family:arial;
  font-size:12px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -o-box-sizing: border-box;
}

  #info .info-wrapper {
    padding:10px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -o-box-sizing: border-box;

  }
  
  #info a {
    color:#c09853;
    text-decoration:none;
  }
  
  #info p {
    margin:5px 0 0 0;
  }


#filters {
  margin:1%;
  padding:0;
  list-style:none;
}

  #filters li {
    float:left;
  }
  
  #filters li span {
    display: block;
    padding:5px 20px;   
    text-decoration:none;
    color:#3498db;
    cursor: pointer;
  }
  
  #filters li span.active {
    background: #3498db;
    color:#fff;
  }
 

 
#portfoliolist .portfolio {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -o-box-sizing: border-box;
  width:23%;
  margin:1%;
  display:none;
  float:left;
  overflow:hidden;
}

  .portfolio-wrapper {
    overflow:hidden;
    position: relative !important;
    background: #666;
    cursor:pointer;
  }

  .portfolio img {
    max-width:100%;
    position: relative;
  }
  
  .portfolio .label {
    position: absolute;
    width: 100%;
    height:40px;
    bottom:-40px;
  }

    .portfolio .label-bg {
      background: #3498db;
      width: 100%;
      height:100%;
      position: absolute;
      top:0;
      left:0;
    }
  
    .portfolio .label-text {
      color:#fff;
      position: relative;
      z-index:500;
      padding:5px 8px;
    }
    .portfolio .text-title{
      color: #fafafc;
      text-decoration: underline
    }
      
      .portfolio .text-category {
        display:block;
        margin-bottom: 3px;
        font-size:9px;
      }
  
  
  



/* #Tablet (Portrait) */
@media only screen and (min-width: 768px) and (max-width: 959px) {
  .container {
    width: 768px; 
  }
}


/*  #Mobile (Portrait) - Note: Design for a width of 320px */
@media only screen and (max-width: 767px) {
  .container { 
    width: 95%; 
  }
  
  #portfoliolist .portfolio {
    width:48%;
    margin:1%;
  }   

  #ads {
    display:none;
  }
  
}


/* #Mobile (Landscape) - Note: Design for a width of 480px */
@media only screen and (min-width: 480px) and (max-width: 767px) {
  .container {
    width: 70%;
  }
  
  #ads {
    display:none;
  }
  
}



/*============== Skill style ==============*/

.skill_area{
  background-color: #f8f8f8;
  
}


.skill_section{
  display: inline-block;
  margin-top: 80px;
  margin-bottom: 60px;
  }

.skill_section h2{
  text-transform: uppercase;
}

.skill_section h2 span{
  color: #3498db;
}

.progress .progress-bar.six-sec-ease-in-out {
    -webkit-transition: width 6s ease-in-out;
    -moz-transition: width 6s ease-in-out;
    -ms-transition: width 6s ease-in-out;
    -o-transition: width 6s ease-in-out;
    transition: width 6s ease-in-out;
}

.skill_text{
  text-align: left;
}


/*============== Contact style ==============*/


.contact_area{
  background: url(img/photography.png);
}

.contact_section{
  margin-top: 60px;
}

.contact_section h1{
  color: #3498db;
  text-transform: uppercase;
  font-weight: 600;
}

.contact_form{

}

#contact_form{ 
    margin-top: 50px; 
    margin-bottom: 30px;  
}

#contact_form legend{
    font-size: 15px; 
    color: #C9C9C9;
}

#contact_form label{
    margin-bottom:28px;
    display: block;
 }

#contact_form label span{
    float:left;
     width:100px; 
     color:#666666;
 }



#contact_form input{
  width: 100%;
  height: 40px;
  border: 1px solid #DBDBDB;
  padding-left: 4px;
 }

#contact_form textarea{
  width: 100%;
  height:100px; 
  padding-left: 4px;
}

.submit_btn { 
  padding: 12px;
  float: left;
  width: 100px;
}

.submit_btn:hover { 

}

.success{ 
    background: #00A243;
    padding: 10px; 
    margin-bottom: 10px; 
    border: 1px solid #B9ECCE; 
    border-radius: 5px; 
    font-weight: normal; 
}
.error{ 
    background: #F44C4C; 
    padding: 10px; 
    margin-bottom: 10px;
     border: 1px solid #FFCACA; 
     border-radius: 5px; 
     font-weight: normal;
 }

  .contact_text{
    text-align: right;
    margin-top: 48px;
 }

 .contact_text p{
    font-size: 15px;
    margin-bottom: 20px;
 }

.contact_text h3{
    text-transform: uppercase;
    font-size: 25px;
  }

.contact_info{
 
  }

.contact_info li{
    list-style: none;
    margin-bottom: 10px;
  }


.contact_social{
    margin-top: 5px;
}

.contact_social li{
    display: inline;
    margin-left: 15px;
} 

.fb:hover{
    color: #3B5998;
}

.tw:hover{
    color: #55acee;
}

.rss:hover{
    color: #f26522;
}

.gp:hover{
    color: #dd4b39;
}

.contact_social {
    font-size: 35px;
}

.map-panel-heading{
  background-color: #bdc3c7;
}

.copyright_section{
  padding-top: 30px;
  margin-bottom: 30px;
  text-align: center;
}


    </style>

    <script>
        $(function() {
            $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>

</head>

<body>
<!--preloader-->
    <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>


<header class="main_header">
  <div class="row">
    <div class="content"> <a class="logo" href="#">JLBWoodart</a>
      <div class="mobile-toggle"> <span></span> <span></span> <span></span></div>
      <nav>
        <ul class="main_menu">
          <li><a href=".hero">Home</a></li>
          <li><a href=".service_area">Service</a></li>
          <li><a href=".portfolio_area">Portfolio</a></li>
          <li><a href=".contact_area">Contact</a></li> 
          <li><a onclick = "location.href = '#openModal1';">Login</a></li> 
          <li>|</li> 
          <li><a onclick = "location.href = '#openModal3';">Sign Up</a></li> 
        </ul>
      </nav>
    </div>
  </div>

  <!-- END row --> 
</header>


<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="5000" id="bs-carousel">
  <!-- Overlay -->
  <div class="overlay"></div>

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#bs-carousel" data-slide-to="1"></li>
    <li data-target="#bs-carousel" data-slide-to="2"></li>
  </ol>
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1"></div>
      <div class="hero">
        <hgroup>
            <h1>We are <span>creative</span></h1>        
            <h3>Get start your next awesome projects</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg"  role="button"  name="btn" onclick = "location.href = '#openModal';">View Catalog</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-2"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are <span>smart</span></h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>       
        <button class="btn btn-hero btn-lg" role="button" name = "btn1"  onclick = "location.href = '#openModal1';" >View Your Profile</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-3"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are <span>amazing</span></h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button" name = "btn2" onclick = "location.href = '#openModal1';">Customize</button>
      </div>
    </div>
  </div> 
</div>






      


<div class="getit_area">
  <div class="container">
    <div class="row">
      <div class="getit_section">
        <h1>The most impressive furniture you'll find.Get it now!</h1>
      </div>  
    </div>
  </div>
</div><!--  getit area end -->

<div class="service_section wow bounceInLeft animated">
<div class="portfolio_area">
  <div class="container">
      <div class="portfolio_section">
          <div class="col-md-12 portfolio_top">
              <h1>Portfolio</h1>
              <p>OUR TEAM INCLUDES GREAT THINKERS. YOU WOULD LOVE TO WORK WITH THEM AS THEY ARE JUST AMAZING PEOPLE.</p>
          </div>  
      </div>

    <ul id="filters" class="clearfix">
      <li><span class="filter active" data-filter="app card icon logos web">All</span></li>
      <li><span class="filter" data-filter="app">Beds</span></li>
      <li><span class="filter" data-filter="card">Chairs</span></li>
      <li><span class="filter" data-filter="icon">Tables</span></li>
      <li><span class="filter" data-filter="logos">Sofas</span></li>
    </ul>

    <div id="portfoliolist">
      <div class="col-md-12 wow bounceInRight animated">
      <div class="portfolio logos" data-cat="logos">
        <div class="portfolio-wrapper">       
          <img src="img/portfolios/logo/newTwilightsofa2.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Twilight Sofa</a>
              <span class="text-category">Sofa</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>        

      <div class="portfolio app" data-cat="app">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/app/bed.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Upholstered Bed</a>
              <span class="text-category">Bed</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>    
          
      
      <div class="portfolio card" data-cat="card">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/card/newcouplechair.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Couple Chair</a>
              <span class="text-category">Chairs</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>  
            
      <div class="portfolio app" data-cat="app">
        <div class="portfolio-wrapper">
          <img src="img/portfolios/app/newbed2.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Widebed</a>
              <span class="text-category">Bed</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>      
      </div>

      <div class="col-md-12 wow bounceInRight animated">
      <div class="portfolio card" data-cat="card">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/card/newdoubleChair.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Double Chair</a>
              <span class="text-category">Chair</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>  
      
      <div class="portfolio card" data-cat="card">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/card/newwoodenoutdoorchair.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Wooden Outdoor Chair</a>
              <span class="text-category">Chair</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>  
      
      <div class="portfolio logos" data-cat="logos">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/logo/sofa.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Simple Sofa</a>
              <span class="text-category">Sofa</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                                                              
      
      <div class="portfolio app" data-cat="app">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/app/newbed3.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">WhiteBed</a>
              <span class="text-category">Bed</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                            
      </div>

      <div class="col-md-12 wow bounceInRight animated">
      <div class="portfolio card" data-cat="card">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/card/newwoodenfolding.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Folding Chair</a>
              <span class="text-category">Chair</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>        

      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/icon/newdesktop.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Mobi Sock</a>
              <span class="text-category">Logo</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                  

                       
      
      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/icon/ClassicTable.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Classic Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>              
                                             

      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/icon/newModern.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Modern Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>        
      </div>

      <div class="col-md-12 wow bounceInRight animated">
      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">
          <img src="img/portfolios/icon/newTrditionalTable.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Tradional Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                                                                                                                  

      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">           
          <img src="img/portfolios/icon/newsmall.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Small Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>      
  
      <div class="portfolio logos" data-cat="logos">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/logo/newbrownsofa.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Lawson-style Sofa</a>
              <span class="text-category">Sofa</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                  

      <div class="portfolio logos" data-cat="logos">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/logo/newwhitesofa.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Sectional Sofa</a>
              <span class="text-category">Sofa</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                                        
      </div>

      <div class="col-md-12 wow bounceInRight animated">
      <div class="portfolio icon" data-cat="icon">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/icon/newContemporary.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Contemporary Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>                                                
      
      <div class="portfolio card" data-cat="card">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/card/3.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Wooden Table</a>
              <span class="text-category">Table</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>  

      <div class="portfolio logos" data-cat="logos">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/logo/newsofa.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">Modern Sofa</a>
              <span class="text-category">Sofa</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>        
           

       <div class="portfolio app" data-cat="app">
        <div class="portfolio-wrapper">     
          <img src="img/portfolios/app/kingsizebed.jpg" alt="" />
          <div class="label">
            <div class="label-text">
              <a class="text-title">King's Size Bed</a>
              <span class="text-category">Bed</span>
            </div>
            <div class="label-bg"></div>
          </div>
        </div>
      </div>           
      </div>
      
    </div>
    
  </div>
</div>
</div>
<!-- portfolio area end   -->

<div class="skill_area">
  <div class="container">
    <div class="row">
      <div class="skill_section">
          <div class="col-md-4">
            
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Why choose us?
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    We designed a variety of new home furnishings, we ensure to provide only the best for our customers by giving them the options they need to make your home a place that reflects your personality. If you wish to furnish an apartment, house or office – you can rely on us to have the perfect fit that will meet you standards and your desire perspective designs. Our inventory covers every possible room you might need to furnish like living room, office, dining, bathroom and kitchen, and bedrooms.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Our History
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Behind Us
                    </a>
                  </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  <div class="panel-body">
                    We offer furniture, specialize furniture and home accessories made of woods (such as Palo China, Mahogany, Jemillina, Tangguille, etc.) and featuring modern materials. We also create design to the creative space for the involvement of the furniture; it also includes the choice of color, material and decoration in our approach with one. We also provide assistance in selecting the right furniture piece and the integration into the existing furniture. Consultation are available at your request, we make sure to that our team will advice or recommends you on your prospect design regarding the furniture that will be exactly appreciated.
                  </div>
                </div>
              </div>
            </div>            
          </div>
          
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:440px;width:700px;'><div id='gmap_canvas' style='height:440px;width:700px;'></div><div><small><a href="http://www.embedgooglemaps.com/en/">Generate your map here, quick and easy!                  Give your customers directions                  Get found</a></small></div><div><small><a href="http://freedirectorysubmissionsites.com/">freedirectorysubmissionsites.com is the most complete web directory list of 2016. Here you can find all directories that have high PR and are search engine friendly. Submitting a link to a link directory is a valuable SEO-strategy, even in 2016. You have to pay attention to the following: if the links you put on a directory are dofollow, otherwise they are of no value to your business.</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(14.695286,121.03178700000001),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(14.695286,121.03178700000001)});infowindow = new google.maps.InfoWindow({content:'<strong>JLB Woodart Furniture</strong><br>C-624 B Quirino Highway Bag-Bag Novaliches Quezon City, Philippines<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
      </div>
    </div>
  </div>
</div><!-- skill area end-->
<div class="contact_area">
  <div class="container">
    <div class="row">
      <div class="contact_section">
          <h1>Contact us</h1>
          <p>OUR TEAM INCLUDES GREAT THINKERS. YOU WOULD LOVE TO WORK WITH THEM AS THEY ARE JUST AMAZING PEOPLE.</p>

                    <div class="col-md-6">
                        <div class="contact_form">
                        <fieldset id="contact_form">
                            <div id="result"></div>
                                <label for="name">
                                <input type="text" name="name" id="name" placeholder="Enter Your Name" />
                                </label>
                                
                                <label for="email">
                                <input type="email" name="email" id="email" placeholder="Enter Your Email" />
                                </label>
                                
                                <label for="phone">
                                <input type="text" name="phone" id="phone" placeholder="Phone Number" />
                                </label>
                                
                                <label for="message">
                                <textarea name="message" id="message" placeholder="Enter Your Name"></textarea>
                                </label>
                                <button class="submit_btn" id="submit_btn" onclick = "location.href = 'contact_me.php';" value = "Goto pornsite">Submit</button>
                                </label>
                        </fieldset>                           
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact_text">
                            
                            <h3>contact info</h3>
                            <ul class="contact_info">
                                <li>jlbwoodart@hotmail.com</li>
                                <li>624 Quirino Highway,Bagbag , Novaliches , Quezon City</li>
                                <li>TIN# 248-242-629,TEL# 02-417-8180</li>
                            </ul>
                            <h3>follow us</h3>
                            <ul class="contact_social">
                                <a href="https://www.facebook.com/jlb.woodart.3?fref=ts&ref=br_tf"><li><i class="fb fa fa-facebook-square"></i></li></a>
                                <a href="#"><li><i class="tw fa fa-twitter-square"></i></li></a>
                            </ul>                            
                        </div>
                    </div>
      </div>
    </div>
  </div> 
</div><!-- contact area end   -->
<!-- PopUp -->
<div class="modalDialog" id="openModal">

    <div class="wrapper">
  <form class="login" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h2><i class="title"></i> Log in</h2>
        <input type="email" placeholder="Email Address" name="email" value="" required="">
        <i class="fa fa-user"></i>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input type="password" placeholder="Password" name="password" required="">
        <i class="fa fa-key"></i>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <button type="submit">Login</button>
    </form>
    
    <div class="foot">
        <a href="#openModal3" class="link">click here to create an account</a>
    </div>
    <a class="close" href="#closed">x</a>
</div>
</div>

<div class="modalDialog" id="openModal1">

  <div class="wrapper">
  <form class="login" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h2><i class="title"></i> Log in</h2>
        <input type="email" placeholder="Email Address" name="email" value="" required="">
        <i class="fa fa-user"></i>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input type="password" placeholder="Password" name="password" required="">
        <i class="fa fa-key"></i>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <button type="submit">Login</button>
    </form>
    
    <div class="foot">
        <a href="#openModal3" class="link">click here to create an account</a>
    </div>
    <a class="close" href="#closed">x</a>
</div>
</div>

<div class = "modalDialog" id = "openModal3">
<div class="wrapper">
<div class="container">

    

    <form class="login" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <h2><i class="fa fa-user"></i> Create account</h2>
        <input type="text" class="form-control" placeholder="Name" name="name" value="" required="">
        <input type="email" placeholder="Email Address" name="email" value="" required="">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input type="text" class="form-control" placeholder="Delivery Address" name="location" value="" required="">
        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
        <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" value="" required="">
        @if ($errors->has('contact_number'))
            <span class="help-block">
                <strong>{{ $errors->first('contact_number') }}</strong>
            </span>
        @endif
        <input type="password" placeholder="Password" name="password" required="">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation"
               required="">
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
        <button type="submit">Register</button>
    </form>

    <div class="foot">
        <a href="#openModal1" class="link">click here if you already have an account</a>
    </div>
</div>
<a class="closes" href="#closed">x</a>
    
</div>
</div>

<!-- End -->


        
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <script src="js/wow.js"></script>
    <script src="js/index.js"></script>

    <script src="js/jquery.nicescroll.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>  
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>

   <script src="js/skillset.js"></script>

   <script src="js/owl.carousel.js"></script> 
   <script type="text/javascript" src="js/main.js"></script>
   

</body>
</html>