<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customize extends Model
{
    protected $table = 'customize';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['prize', 'description'];
}
