<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\ProductRepositoryInterface', 'App\Repositories\ProductRepository');
        $this->app->bind('App\Repositories\ProfileRepositoryInterface', 'App\Repositories\ProfileRepository');
        $this->app->bind('App\Repositories\OrderRepositoryInterface', 'App\Repositories\OrderRepository');
        $this->app->bind('App\Repositories\ReservationRepositoryInterface', 'App\Repositories\ReservationRepository');
        $this->app->bind('App\Repositories\CustomRepositoryInterface', 'App\Repositories\CustomRepository');
    }
}
