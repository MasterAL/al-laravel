<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('/', 'WelcomeController@index');

Route::get('/login', function () {
    return view('login');
});


Route::get('/register', function () {
    return view('register');
});

Route::resource('admin', 'AdminController');
Route::resource('product', 'ProductController');
Route::resource('product/update', 'ProductController@updateProduct');
Route::resource('order', 'OrderController');
Route::resource('reservation', 'ReservationController');
Route::resource('custom', 'CustomController');
Route::delete('profile/{id}', 'ProfileController@destroy');
Route::post('customize', 'CustomizeController@create');

Route::auth();

Route::get('/home', 'HomeController@index');
Route::post('/home/update', 'HomeController@update');

Route::post('authenticate', 'Auth\AuthenticateController@authenticate');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::post('auth/register', 'Auth\AuthController@postRegister');

