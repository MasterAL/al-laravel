<?php

namespace App\Http\Controllers;

use App\Repositories\CustomRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;

class CustomController extends Controller
{

    private $custom;

    public function __construct(CustomRepositoryInterface $customRepository)
    {
        $this->custom = $customRepository;
    }

    public function store(Request $request){


        if ($request->has("enable")) {
            $enable = $request->input('enable');
        }
        if ($request->has("prize")) {
            $prize = $request->input('prize');
        }
        if ($request->has("id")) {
            $id = $request->input('id');
        }

        $this->custom->update($id,$prize,$enable);

        return redirect("admin");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->custom->findById($id);
        return response()->json(['custom' => $data]);
    }
}
