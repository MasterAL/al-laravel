<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProfileRepositoryInterface;

use App\Http\Requests;

class ProfileController extends Controller
{

    private $profile;

    public function __construct( ProfileRepositoryInterface $profile)
    {
        $this->profile = $profile;
    }

    public function destroy($id){
        return response()->json(['status' => $this->profile->delete($id)]);
    }
}
