<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\ProfileRepositoryInterface;
use App\Repositories\ReservationRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;
use App\Repositories\CustomRepositoryInterface;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $product;
    private $profile;
    private $reservation;
    private $order;
    private $custom;

    public function __construct(ProductRepositoryInterface $product,
                                ProfileRepositoryInterface $profile,
                                ReservationRepositoryInterface $reservation,
                                OrderRepositoryInterface $order,
                                CustomRepositoryInterface $custom)
    {
        $this->product = $product;
        $this->profile = $profile;
        $this->reservation = $reservation;
        $this->order = $order;
        $this->custom = $custom;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->isadmin == 1){
            return redirect('admin');
        }
        $user = $request->user();
        $profile = $this->profile->findByUserId($user->id);
        $guitar = $this->product->findBy("category","Guitar");
        $headset = $this->product->findBy("category","Headset");
        $reservations = $this->reservation->getAll();
        $reservation = $this->reservation->findBy("userid",$user->id);
        $order = $this->order->getAll();
        $body = $this->custom->findBy("type","Body");
        $pickguard = $this->custom->findBy("type","Pickguard");
        $fretboard = $this->custom->findBy("type","Fretboard");
        $bridge = $this->custom->findBy("type","Bridge");
        return view("home",[
            'user' => $user,
            'profile' => $profile,
            'guitars' => $guitar,
            'headsets' => $headset,
            'reservations' => $reservations,
            'reservation' => $reservation,
            'orders' => $order,
            'bodies' => $body,
            'pickguards' => $pickguard,
            'fretboards' => $fretboard,
            'bridges' => $bridge
        ]);
    }

    public function update(Request $request){
        $user = $request->user();

        if ($request->has("email")) {
             $email = $request->input('email');
        }

        if ($request->has("address")) {
             $address = $request->input('address');
        }

        if ($request->has("password")) {
             $password = $request->input('password');
        }

        if ($request->has("number")) {
             $number = $request->input('number');
        }

        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

        $profile = $this->profile->findByUserId($user->id);
        $profile->address = $address;
        $profile->contactnumber = $number;
        $profile->save();

        return redirect("home");
    }

}
