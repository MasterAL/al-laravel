<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


use App\Http\Requests;

class ProductController extends Controller
{
    private $product;

    public function __construct(ProductRepositoryInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has("name")) {
            $name = $request->input('name');
        }
        if ($request->has("prize")) {
            $prize = $request->input('prize');
        }
        if ($request->hasFile("pic")) {
            $pic = $request->file('pic');
            $imageName = uniqid() . '.' .
                $pic->getClientOriginalExtension();
            Storage::disk('image')->put($imageName, File::get($pic));
            $filepath = "/imgs/products/" . $imageName;
        }
        if ($request->has("stock")) {
            $stock = $request->input('stock');
        }
        if ($request->has("category")) {
            $category = $request->input('category');
        }
        if ($request->has("description")) {
            $description = $request->input('description');
        }

        $product = $this->product->create($name, $prize, $filepath, $category, $description,$stock);

        return redirect("/admin");

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->product->findById($id);
        return response()->json(['product' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has("name")) {
            $name = $request->input('name');
        }
        if ($request->has("prize")) {
            $prize = $request->input('prize');
        }
        if ($request->has("stock")) {
            $stock = $request->input('stock');
        }
        if ($request->hasFile("pic")) {
            $pic = $request->file('pic');
            $imageName = uniqid() . '.' .
                $pic->getClientOriginalExtension();
            Storage::disk('image')->put($imageName, File::get($pic));
            $filepath = "/imgs/products/" . $imageName;
        }
        if ($request->has("category")) {
            $category = $request->input('category');
        }


        echo $product = $this->product->update($id, $name, $prize, $filepath, $category,$stock);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->product->delete($id);
    }

    public function updateProduct(Request $request)
    {
        if ($request->has("edit_name")) {
            $name = $request->input('edit_name');
        }
        if ($request->has("edit_prize")) {
            $prize = $request->input('edit_prize');
        }
        if ($request->hasFile("edit_pic")) {
            $pic = $request->file('edit_pic');
            $imageName = uniqid() . '.' .
                $pic->getClientOriginalExtension();
            Storage::disk('image')->put($imageName, File::get($pic));
            $filepath = "/imgs/products/" . $imageName;
        }else{
            $filepath = null;
        }
        if ($request->has("edit_category")) {
            $category = $request->input('edit_category');
        }
        if ($request->has("edit_id")) {
            $id = $request->input('edit_id');
        }
        if ($request->has("stock")) {
            $stock = $request->input('stock');
        }
        if ($request->has("description")) {
            $description = $request->input('description');
        }


        echo $product = $this->product->update($id, $name, $prize, $filepath, $category, $stock,$description);

       return redirect("/admin");

    }
}
