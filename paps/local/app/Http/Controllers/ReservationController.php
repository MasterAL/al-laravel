<?php

namespace App\Http\Controllers;

use App\Repositories\ReservationRepositoryInterface;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;


use App\Http\Requests;

class ReservationController extends Controller
{
    private $reservation;
    private $paypal;

    public function __construct(ReservationRepositoryInterface $reservation)
    {
        $this->reservation = $reservation;
        $this->paypal = new ApiContext(
            new OAuthTokenCredential(
                'AZjCLYJVewcNATtQOx9LHq_XnAFFSPlJkiU5OYWu9aezPKPXreKpb1GONHnxCRAyIXkUs1Qb7gJIE8OS',
                'EFLcxFT2m-DEVDgIgRqA5x_KbRHPkJLk40qLE6VHlRQUy7sPGtqxW1711S_v6Se8vi8FCefmv9A6wiZw'
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has("date")) {
            $date = $request->input('date');
            $newDate = date("Y-m-d", strtotime($date));
        }
        $reservations = $this->reservation->findByWithUser("date", $newDate);
        return response()->json(['reservations' => $reservations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->user()->id;
        if ($request->has("date")) {
            $date = $request->input('date');
            $newDate = date("Y-m-d", strtotime($date));
        }
        if ($request->has("starttime")) {
            $starttime = $request->input('starttime');
        }
        if ($request->has("endtime")) {
            $endtime = $request->input('endtime');
        }
        if ($request->has("prize")) {
             $prize = $request->input('prize');
        }
        $data = $this->reservation->create($id, $newDate, $starttime, $endtime);
        // return response()->json(['status'=>'ok','order' => $data]);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $prize = preg_replace('/&.*?;/', '', $prize) / 2;


        $item = new Item();
        $item->setName("Studio Reservation")
            ->setCurrency('PHP')
            ->setQuantity(1)
            ->setPrice($prize);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setSubtotal($prize);

        $amount = new Amount();
        $amount->setCurrency('PHP')
            ->setTotal($prize)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Knoxville')
            ->setInvoiceNumber($data->id);

        $redirectUrls = new redirectUrls();
        $redirectUrls->setReturnUrl("http://giljan-001-site1.gtempurl.com/home")
            ->setCancelUrl("http://giljan-001-site1.gtempurl.com/home");

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->paypal);
        } catch (Exception $e) {
            die($e);
        }

        return redirect($payment->getApprovalLink());

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($date)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
