<?php

namespace App\Http\Controllers;

use App\Customize;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use App\Http\Requests;

class CustomizeController extends Controller
{
    private $order;
    private $paypal;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->order = $orderRepository;
        $this->paypal = new ApiContext(
            new OAuthTokenCredential(
                'AZjCLYJVewcNATtQOx9LHq_XnAFFSPlJkiU5OYWu9aezPKPXreKpb1GONHnxCRAyIXkUs1Qb7gJIE8OS',
                'EFLcxFT2m-DEVDgIgRqA5x_KbRHPkJLk40qLE6VHlRQUy7sPGtqxW1711S_v6Se8vi8FCefmv9A6wiZw'
            )
        );
    }

    public function create(Request $request)
    {

        $id = $request->user()->id;

        if ($request->has("prize")) {
            $prize = $request->input('prize');
        }

        if ($request->has("description")) {
            $description = $request->input('description');
        }

        if ($request->has("payment")) {
            $payment = $request->input('payment');
        }



        $customize = new Customize();
        $customize->prize = $prize;
        $customize->description = $description;
        $customize->save();

        $data = $this->order->create($id, 0, $customize->id, "Ongoing");

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        if ($payment != "full"){
            $prize = $prize / 2;
        }

        $item = new Item();
        $item->setName("Custom Guitar")
            ->setCurrency('PHP')
            ->setQuantity(1)
            ->setPrice($prize);


        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setSubtotal($prize);

        $amount = new Amount();
        $amount->setCurrency('PHP')
            ->setTotal($prize)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Knoxville')
            ->setInvoiceNumber($data->ordernum);

        $redirectUrls = new redirectUrls();
        $redirectUrls->setReturnUrl("http://giljan-001-site1.gtempurl.com/home")
            ->setCancelUrl("http://giljan-001-site1.gtempurl.com/home");

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->paypal);
        } catch (Exception $e) {
            die($e);
        }
        return response()->json(['status' => 'ok', 'link' =>  $payment->getApprovalLink()]);
    }

}
