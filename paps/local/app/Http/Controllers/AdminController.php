<?php

namespace App\Http\Controllers;

use App\Repositories\CustomRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\ProfileRepositoryInterface;
use App\Repositories\ReservationRepositoryInterface;
use App\Repositories\OrderRepositoryInterface;

use App\Http\Requests;

class AdminController extends Controller
{
    private $product;
    private $profile;
    private $reservation;
    private $order;
    private $custom;

    public function __construct(ProductRepositoryInterface $product,
                                ProfileRepositoryInterface $profile,
                                ReservationRepositoryInterface $reservation,
                                OrderRepositoryInterface $order,
                                CustomRepositoryInterface $custom)
    {
        $this->product = $product;
        $this->profile = $profile;
        $this->reservation = $reservation;
        $this->order = $order;
        $this->custom = $custom;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $profile = $this->profile->getAll();
        $guitar = $this->product->findBy("category","Guitar");
        $headset = $this->product->findBy("category","Headset");
        $reservation = $this->reservation->getAll();
        $order = $this->order->getAll();
        $body = $this->custom->findBy("type","Body");
        $pickguard = $this->custom->findBy("type","Pickguard");
        $fretboard = $this->custom->findBy("type","Fretboard");
        $bridge = $this->custom->findBy("type","Bridge");
        return view("admin",[
            'user' => $user,
            'profiles' => $profile,
            'guitars' => $guitar,
            'headsets' => $headset,
            'reservations' => $reservation,
            'orders' => $order,
            'bodies' => $body,
            'pickguards' => $pickguard,
            'fretboards' => $fretboard,
            'bridges' => $bridge
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
