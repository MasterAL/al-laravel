<?php

namespace App\Repositories;

interface ProfileRepositoryInterface{

    public function getAll();

    public function findByUserId($id);

    public function delete($id);

    public function findBy($att, $column);

    public function get($offset,$limit);

}
