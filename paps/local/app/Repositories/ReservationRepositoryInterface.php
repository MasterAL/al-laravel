<?php

namespace App\Repositories;

interface ReservationRepositoryInterface
{

    public function getAll();

    public function findById($id);

    public function findBy($att, $column);

    public function findByWithUser($column, $att);

    public function get($offset, $limit);

    public function create($userid, $date, $starttime, $endtime);

}
