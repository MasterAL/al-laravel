<?php

namespace App\Repositories;


use App\Reservation;

class ReservationRepository implements ReservationRepositoryInterface
{

    private $reservation;

    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    public function getAll()
    {
        return $this->reservation->all();
    }

    public function findById($id)
    {
        return $this->reservation->find($id);
    }

    public function findBy($column, $att)
    {
        return $this->reservation->where($column, $att)->get();
    }

    public function findByWithUser($column, $att)
    {
        return $this->reservation->where($column, $att)->with("user")->get();
    }

    public function get($offset, $limit)
    {
        return $this->reservation->limit($limit)->offset($offset)->get();
    }

    public function create($userid, $date, $starttime, $endtime)
    {
        $reservation = new Reservation();

        $reservation->userid = $userid;
        $reservation->date = $date;
        $reservation->starttime = $starttime;
        $reservation->endtime = $endtime;

        $reservation->save();

        return $reservation;
    }
}