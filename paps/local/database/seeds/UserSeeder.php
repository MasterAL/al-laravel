<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(array('id' => '1','email' => 'admin@gmail.com' , 'password' => bcrypt("123456"), 'isadmin' => 1));
        User::create(array('id' => '2','email' => 'user@gmail.com' , 'password' => bcrypt("123456") , 'isadmin' => 0));
    }
}
