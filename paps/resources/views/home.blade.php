<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>JLB - Account</title>
    <link rel="icon" href="img/pro.jpg" type="image/png">
    <link href="css/account.css" rel="stylesheet" type="text/css">
    <link href="css/font.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/print.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">

        var isCustom;
        var customDescription;
        var customTotal;
        // document.addEventListener('DOMContentLoaded', done);
        window.onload = function () {
            // document.getElementsByClassName("wrapper")[0].style.display = "inline";
            showGroup("transactions");
        };

        // function done(){
        //     var set1 = setInterval(function(){
        //         clearInterval(set1);
        //         showGroup("account");
        //         document.getElementById("loading").style.opacity = 0;
        //         document.getElementsByClassName("wrapper")[0].style.display = "inline";
        //     }, 500);
        // }

        function showGroup(nav) {
            var o = document.getElementsByClassName("group");
            for (var i = 0; i < o.length; i++) {
                if (nav == o[i].id || (nav == "products" && o[i].id == "add-prod")) {
                    o[i].style.display = "inline-block";
                } else {
                    o[i].style.display = "none";
                }
            }
        }

        function showCat(cat) {
            var o = document.getElementsByClassName("category");
            for (var i = 0; i < o.length; i++) {
                if (cat == o[i].id) {
                    o[i].className = "category";
                } else {
                    o[i].className = "category hide";
                }
            }
        }

        var cart = [];
        function addItem(itemid,stock) {
            var name = document.getElementById(itemid).innerHTML;
            var price = parseInt(document.getElementById(itemid + "-prc").innerHTML);
            var qty = parseInt(document.getElementById(itemid + "-qty").value);
            if(parseInt(qty)>stock){
                alert("Not enough stock.");
            }else{
                document.getElementById(itemid + "-qty").value = "0";
                var amt = qty * price;
                var exist = 0;
                if (qty > 0) {
                    if (cart.length > 0) {
                        for (var i = 0; i < cart.length; i++) {
                            if (cart[i].indexOf(itemid) >= 0) {
                                exist = 1;
                                cart[i][3] += qty;
                                cart[i][4] = cart[i][2] * cart[i][3];
                                i = cart.length;
                            }
                        }
                    }
                    if (exist == 0) {
                        cart.push([itemid, name, price, qty, amt]);
                    }
                    insertItem();
                }
            }


        }

        function insertItem() {
            var tbl = document.getElementById("cart-items");
            var totTxt = document.getElementById("cart-totamt");
            var totAmt = 0;
            tbl.innerHTML = "";
            for (var i = 0; i < cart.length; i++) {
                tbl.innerHTML += '<tr><td><i class="fa fa-times" onclick=\'removeItem("' + cart[i][0] + '")\'></i> ' + cart[i][1] + '</td><td>' + cart[i][3] + '</td><td>' + cart[i][4] + '.00</td></tr>';
                totAmt += parseInt(cart[i][4]);
                totTxt.innerHTML = totAmt;
            }
        }

        function removeItem(itemid) {
            for (var i = 0; i < cart.length; i++) {
                if (cart[i][0] == itemid) {
                    cart.splice(i, 1);
                }
            }
            if (cart.length > 0) {
                insertItem();
            } else {
                clearItems();
            }
        }

        function clearItems() {
            var tbl = document.getElementById("cart-items").innerHTML = '<tr><td>No Items</td><td></td><td></td></tr>';
            var totTxt = document.getElementById("cart-totamt").innerHTML = "0";
            cart = [];
        }

        function showCartCO(type) {
            var tbl = document.getElementById("cart-co-items");
            var totTxt = document.getElementById("cart-co-totamt");
            var cBtn = document.getElementById("cBtn");
            var oBtn = document.getElementById("oBtn");
            var totAmt = 0;
            tbl.innerHTML = "";
            if (type == "custom") {
                if (totprice > 250) {
                    var c1 = document.querySelector('input[name="body"]:checked').value;
                    var c2 = document.querySelector('input[name="pickguard"]:checked').value;
                    var c3 = document.querySelector('input[name="fretboard"]:checked').value;
                    var c4 = document.querySelector('input[name="bridge"]:checked').value;
                    totAmt = totprice;
                    totTxt.innerHTML = totAmt;
                    customDescription = 'Custom Item ('
                            + "Body: " + c1 + ", "
                            + "Pickguard: " + c2 + ", "
                            + "Fretboard: " + c3 + ", "
                            + "Bridge: " + c4
                            + ')';
                    customTotal = totAmt;
                    tbl.innerHTML += '<tr><td>'+customDescription+'</td><td>1</td><td>' + totAmt + '.00</td></tr>';
                    showGroup("cart-checkout");
                } else {
                    alert("Choose a design");
                }
                isCustom = true;
            } else {
                if (cart.length > 0) {
                    for (var i = 0; i < cart.length; i++) {
                        tbl.innerHTML += '<tr><td>' + cart[i][1] + '</td><td>' + cart[i][3] + '</td><td>' + cart[i][4] + '.00</td></tr>';
                        totAmt += parseInt(cart[i][4]);
                        totTxt.innerHTML = totAmt;
                    }
                    showGroup("cart-checkout");
                }
                isCustom = false;
            }
            cBtn.attributes[1].value = 'hideCartCO("' + type + '")';
            oBtn.attributes[1].value = 'order("' + type + '")';
        }

        function hideCartCO(type) {
            if (type == "custom") {
                showGroup("build");
            } else {
                showGroup("products");
            }
        }

        function showTY(id) {
            showGroup("cart-ty");
            document.getElementById("ordernum").innerHTML = "#" + id;
        }

        function order() {
            var totAmt = 0;
            if (isCustom) {
                $.post('customize', {
                    description: customDescription,
                    prize: customTotal,
                    payment : $('input[name="r-p"]:checked').val()
                }, function (data) {
                    console.log(data["link"]);
                    window.location = data["link"];
                }).fail(function (xhr, status, error) {
                    console.log(error);
                });

            } else {
                for (var i = 0; i < cart.length; i++) {
                    totAmt += parseInt(cart[i][4]);
                }
                for (var i = 0; i < cart.length; i++) {
                    console.log(parseInt(cart[i][0]));
                    console.log($('input[name="r-p"]:checked').val());
                    $.post('order', {
                        productid: parseInt(cart[i][0]),
                        payment : $('input[name="r-p"]:checked').val()
                    }, function (data) {
                        console.log(data["link"]);
                        window.location = data["link"];
                    }).fail(function (xhr, status, error) {
                        console.log(error);
                    });
                }


            }
        }

        function selectTime(time) {
            var t = document.getElementById(time);
            if (t.className != "selected-time") {
                var idx = document.getElementsByClassName("selected-time").length;
                if (idx < 2) {
                    idx++;
                    t.className = "selected-time";
                    t.dataset.selection = idx;
                    t.checked = true;
                } else {
                    // var times = document.getElementsByName("time");
                    var times = document.querySelectorAll('input[name="time"]:enabled');
                    var num = 0;
                    for (var i = 0; i < times.length; i++) {
                        if (parseInt(times[i].dataset.selection) != 1) {
                            times[i].className = "";
                            times[i].dataset.selection = 0;
                            times[i].checked = false;
                        }
                    }
                    t.className = "selected-time";
                    t.dataset.selection = idx;
                    t.checked = true;
                }
            } else {
                t.className = "";
                t.dataset.selection = 0;
            }
            resetTimeSelection();
            setTime();
        }

        function resetTimeSelection() {
            // var times = document.getElementsByName("time");
            var times = document.querySelectorAll('input[name="time"]:enabled');
            var idx = document.getElementsByClassName("selected-time").length;
            var inside = 0;
            var halt = false;
            var direction;
            var startt;
            var endt;
            if (idx < 2) {
                for (var i = 0; i < times.length; i++) {
                    if (parseInt(times[i].dataset.selection) > 0) {
                        times[i].className = "selected-time";
                        times[i].dataset.selection = 1;
                        times[i].checked = true;
                    } else {
                        times[i].className = "";
                        times[i].dataset.selection = 0;
                        times[i].checked = false;
                    }
                }
            } else {
                for (var i = 0; i < times.length; i++) {
                    if (times[i].dataset.selection == 1) {
                        startt = parseInt(times[i].dataset.sortorder);
                    } else if (times[i].dataset.selection == 2) {
                        endt = parseInt(times[i].dataset.sortorder);
                    }
                }
                if (startt > endt) {
                    direction = "reverse";
                } else {
                    direction = "forward"
                }

                if (direction == "forward") {
                    for (var i = 0; i < times.length; i++) {
                        if ((parseInt(times[i].dataset.selection) > 0 && !halt) || (inside == 1 && times[i].disabled)) {
                            inside++;
                        }
                        if (inside > 0 && inside <= 2) {
                            times[i].checked = true;
                            if (inside == 2) {
                                inside = 0;
                                if (times[i].disabled) {
                                    halt = true;
                                    if (times[i - 1].dataset.selection != 1) {
                                        times[i - 1].className = "selected-time";
                                        times[i - 1].dataset.selection = 2;
                                        times[i - 1].checked = true;
                                    }
                                }
                            }
                        } else {
                            times[i].className = "";
                            times[i].dataset.selection = 0;
                            times[i].checked = false;
                        }
                    }
                } else if (direction == "reverse") {
                    for (var i = times.length - 1; i >= 0; i--) {
                        if ((parseInt(times[i].dataset.selection) > 0 && !halt) || (inside == 1 && times[i].disabled)) {
                            inside++;
                        }
                        if (inside > 0 && inside <= 2) {
                            times[i].checked = true;
                            if (inside == 2) {
                                inside = 0;
                                if (times[i].disabled) {
                                    halt = true;
                                    if (times[i + 1].dataset.selection != 1) {
                                        times[i + 1].className = "selected-time";
                                        times[i + 1].dataset.selection = 2;
                                        times[i + 1].checked = true;
                                    }
                                }
                            }
                        } else {
                            times[i].className = "";
                            times[i].dataset.selection = 0;
                            times[i].checked = false;
                        }
                    }
                }
            }
        }

        function setTime() {
            var t = document.getElementsByClassName("selected-time");
            var st = document.getElementById("st");
            var et = document.getElementById("et");
            var pr = document.getElementById("pr");
            var prQty = document.querySelectorAll('input[name="time"]:checked').length;
            var prBase = 200;
            var prPromo = 180;
            if (t.length == 1) {
                st.value = t[0].dataset.val
                if (t[0].id == "time16") {
                    et.value = "24:00";
                } else {
                    et.value = document.getElementById(t[0].id).parentNode.nextSibling.nextSibling.childNodes[0].dataset.val;
                }
                pr.value = prBase;
            } else if (t.length == 2) {
                st.value = t[0].dataset.val;
                if (t[1].id == "time16") {
                    et.value = "24:00";
                } else {
                    et.value = t[1].parentNode.nextSibling.nextSibling.childNodes[0].dataset.val;
                }
                if(prQty%2 == 0){
                    pr.value = parseInt(prPromo * prQty);
                }else{
                    pr.value = parseInt((prPromo * (prQty-1)) + prBase);
                }
            } else {
                st.value = "";
                et.value = "";
                pr.value = "";
            }
        }

        function changeDate() {
            var date = document.getElementById("date");
            var timeEle = document.getElementsByName("time");

            for (var i = 0; i < timeEle.length; i++) {
                timeEle[i].disabled = false;
                timeEle[i].className = "";
                timeEle[i].dataset.selection = 0;
                timeEle[i].checked = false;
            }

            // console.log(date.value);
            $.get('reservation?date=' + date.value,
                    function (data) {
                        var reservations = data["reservations"];
                        for (var i = 0; i < reservations.length; i++) {
                            var obj = reservations[i];
                            var startTime = obj.starttime.split(":")[0];
                            var endTime = parseInt(obj.endtime.split(":")[0]);
                            var incTime = parseInt(startTime);
                            var timeEleSel = document.querySelectorAll('[data-val="' + startTime + ':00"]')[0];
                            var x = timeEleSel.dataset.sortorder - 1;
                            while (incTime < endTime) {
                                timeEle[x].disabled = true;
                                timeEle[x].className = "";
                                x++;
                                incTime++;
                            }
                        }
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
            disableTime();
        }

        function disableTime(){
            var timeEle = document.querySelectorAll('input[name="time"]:enabled');
            var dateNow = new Date();
            var month = ["01","02","03","04","05","06","07","08","09","10","11","12"];
            var date = document.getElementById("date").value;
            var utcDate = Date.UTC(date.split("-")[0],date.split("-")[1],date.split("-")[2]);
            var utcDateNow = Date.UTC(dateNow.getFullYear(), month[dateNow.getMonth()], dateNow.getDate());
            if (utcDate == utcDateNow) {
                for (var i = 0; i < timeEle.length; i++) {
                    if (dateNow.getHours() >= timeEle[i].dataset.val.split(":")[0]) {
                        timeEle[i].disabled = true;
                        timeEle[i].className = "na";
                    }
                }
            }else if (utcDate < utcDateNow){
                for (var i = 0; i < timeEle.length; i++) {
                    timeEle[i].disabled = true;
                    timeEle[i].className = "na";
                }
            }
        }

        function validateForm() {
            var x = document.forms["form"]["starttime"].value;
            var y = document.forms["form"]["endtime"].value;
            if (x == null || x == "") {
                alert("Start time must be filled out");
                return false;
            }
            if (y == null || y == "") {
                alert("End time must be filled out");
                return false;
            }
        }

        function validatePassword() {
            var x = document.forms["form"]["password"].value;
            var y = document.forms["form"]["cpassword"].value;
            if (x == null || x == "") {
                alert("Password must be filled out");
                return false;
            }
            if (y == null || y == "") {
                alert("Confirm Password must be filled out");
                return false;
            }
            if (y != x) {
                alert("Passwords not equal");
                return false;
            }
        }




        function togglePayBtn(status) {
            if (status == "Ongoing") {
                document.getElementById("trans-o-paybtn").disabled = false;
            } else {
                document.getElementById("trans-o-paybtn").disabled = true;
            }
        }

        function printPage(page) {
            document.getElementById("OR").style.display = "none";
            document.getElementById("Guitar").style.display = "none";
            document.getElementById(page).style.display = "block";
            if (page == "OR") {
                document.getElementById("print-ordernum").innerHTML = document.getElementById("trans-o-num").innerHTML;
                // document.getElementById("print-userid").innerHTML = document.getElementById("order_email").dataset.userid;
                // document.getElementById("print-email").innerHTML = document.getElementById("order_email").value;
                document.getElementById("print-order-date").innerHTML = document.getElementById("order_date").value;
                document.getElementById("print-or-t").innerHTML = document.getElementById("orderTable").innerHTML;
                var cash = document.getElementById("print-cash");
                // cash.innerHTML = document.getElementById("trans-o-num").dataset.cash;
                // var amount = document.getElementById("print-amount");
                // amount.innerHTML = document.getElementById("trans-o-num").dataset.amount;
                // var change = document.getElementById("print-change"); 
                // change.innerHTML = parseInt(cash.innerHTML.split(".")[0]) - parseInt(amount.innerHTML.split(".")[0]);
            } else if (page == "Guitar"){
                var src = document.getElementById("template");
                // var dst = document.getElementById("destCanvas");
                // var dstctx = dst.getContext('2d');
                // dstctx.drawImage(src,0,0);
                document.getElementById("cnvsImg").src=src.toDataURL("image/png");
            }
            window.print();
        }


        function getOrder(ordernum) {
            var order_date = document.getElementById("order_date");
            var order_status = document.getElementById("order_status");
            var order_estimate = document.getElementById("order_estimate");
            var order_body = document.getElementById("orderTable").getElementsByTagName('tbody')[0];
            var num = document.getElementById("trans-o-num");
            order_body.innerHTML = "";
            console.log("ordernum value : " + ordernum);
            $.get('order?ordernum=' + ordernum,
                    function (data) {
                        var orders = data["orders"];
                        for (var i = 0; i < orders.length; i++) {
                            var obj = orders[i];
                            console.log(obj);
                            num.innerHTML = obj.ordernum;
                            order_date.value = obj.created_at;
                            var bits = obj.created_at.split(/\D/);
                            var date = new Date(bits[0], --bits[1], bits[2], bits[3], bits[4]);
                            var day = date.getDate();
                            var monthIndex = date.getMonth();
                            var year = date.getFullYear();

                            order_estimate.value = year + '-' + pad(monthIndex+1) + '-' +day ;
                            order_status.value = obj.status;
                            var row = order_body.insertRow(i);
                            var c1 = row.insertCell(0);
                            var c2 = row.insertCell(1);
                            var c3 = row.insertCell(2);
                            if (obj.product != null){
                                c1.innerHTML = obj.product.name;
                                c2.innerHTML = obj.product.category;
                                c3.innerHTML = "₱" + obj.product.prize;
                                num.dataset.amount = obj.product.prize;
                            }else{
                                c1.innerHTML = "Custom Guitar";
                                c2.innerHTML = "Custom";
                                c3.innerHTML = "₱" + obj.custom.prize;
                                num.dataset.amount = obj.custom.prize;
                            }


                        }
                    }).fail(function (xhr, status, error) {
                console.log(error);
            });
        }


        function pad(d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        }

    </script>
</head>

<body>

<!-- <div id="loading">
    <div class="loader">Loading...</div>
</div> -->

<div class="wrapper">

    <nav id="navbar" class="navbar-collapse">
        <div class="container">

            <div id="logo-container">
                <span id="logo"></span>
                <a id="logo-link" href="/">JLB Woodart</a>
            </div>

            <div id="user" onclick='showGroup("account");'>
                <span id="pic"></span>
                <div class="user-detail">
                    <h3>User</h3>
                    <i class="fa fa-refresh"></i><span>Update</span>
                </div>
            </div>

            <div class="option" onclick='showGroup("transactions");'>
                <i class="fa fa-table"></i>
                <span>Transactions</span>
            </div>
            <div class="option" onclick='showGroup("products");'>
                <i class="fa fa-book"></i>
                <span>Order</span>
            </div>
            <!--<div class="option" onclick='showGroup("reserve-list"); changeDate();'>
                <i class="fa fa-calendar"></i>
                <span>Reserve</span>
            </div> -->
            <div class="option" onclick='location.replace("http://customize.esy.es/local/");'>
                <i class="fa fa-puzzle-piece"></i>
                <span>Build</span>
            </div>

            <a href="{{ url('/logout') }}">
                <div id="nav-fabar">
                    <i class="fa fa-sign-out"></i>
                </div>
            </a>

        </div>
    </nav>

    <div id="content">
        <div id="transactions" class="group trans">
            <h1><i class="fa fa-table"></i> Transactions</h1>
            <div class="trans-content">
                <table class="trans-tbl-main">
                    <thead>
                    <td>Transaction Date</td>
                    <td>Transaction #</td>
                    <td>Customer</td>
                    <td>Status</td>
                    </thead>
                    @foreach($orders as $order)
                        <tr onclick='showGroup("trans-order"); getOrder({{$order->ordernum}}); togglePayBtn("{{$order->status}}")'>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->ordernum}}</td>
                            <td>{{$order->user->email}}</td>
                            <td>{{$order->status}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div id="trans-order" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Order #<span id="trans-o-num">123</span>
                <span class="trans-closebtn" onclick='showGroup("transactions")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Order Date</h2>
                <input id="order_date" type="text" value="2016-09-03" readonly="">
                <h2>Order Status</h2>
                <input id="order_status" type="text" value="Complete" readonly="">
                <h2>Estimated Completion Date</h2>
                <input id="order_estimate" type="text" readonly="" value="">
                <table id="orderTable">
                    <thead>
                    <td>Product</td>
                    <td>Category</td>
                    <td>Price</td>
                    </thead>
                    <tr>
                        <td>Guitar1</td>
                        <td>Guitar</td>
                        <td>₱ 0.00</td>
                    </tr>
                </table>
                <div class="add-btn">
                    <button id="trans-o-paybtn"><i class="fa fa-paypal"></i> Pay Now</button>
                    <button type="button" onclick='printPage("OR")'><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>

        <div id="trans-reserve" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Reservation #<span id="trans-r-num">123</span>
                <span class="trans-closebtn" onclick='showGroup("reserve-list")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Reservation Date</h2>
                <input type="date" value="2016-09-03" readonly="">
                <table>
                    <thead>
                    <td>Start Time</td>
                    <td>End Time</td>
                    </thead>
                    <tr>
                        <td>00:00</td>
                        <td>00:00</td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="trans-custom" class="group trans">
            <h1>
                <i class="fa fa-book"></i> Customization Order #<span id="trans-c-num">123</span>
                <span class="trans-closebtn" onclick='showGroup("transactions")'><i
                            class="fa fa-times-circle"></i></span>
            </h1>
            <div class="trans-content">
                <h2>Order Date</h2>
                <input type="date" value="2016-09-03" readonly="">
                <h2>Order Status</h2>
                <input type="text" value="Complete" readonly="">
            </div>
        </div>

        <div id="account" class="group">
            <form enctype="multipart/form-data"  name="form" method="post" action="/home/update" onsubmit="return validatePassword();">
                {{ csrf_field() }}
            <h1><i class="fa fa-user"></i> Account Info</h1>
            <div class="acc-content">
                <h2>Email Address</h2>
                <input type="email" name="email" value="{{$user->email}}">
                <h2>Delivery Address</h2>
                <input type="text" name="address" value="{{$user->profile->address}}">
                <h2>Contact Number</h2>
                <input type="text" name="number" value="{{$user->profile->contactnumber}}">
                <h2>Password</h2>
                <input name="password" type="password" >
                <h2>Confirm Password</h2>
                <input name="cpassword" type="password" >
                <div class="add-btn">
                    <button onclick='showGroup("transactions")'><i class="fa fa-times"></i> Cancel</button>
                    <button type="submit" id="acc-updatebtn" ><i
                                class="fa fa-check"></i> Update
                    </button>
                </div>
            </div>
                </form>
        </div>

        <div id="products" class="group">
            <h1><i class="fa fa-book"></i> Products</h1>

            <div id="categories">
                <div class="option" data-itemcategory="cat1" onclick="showCat(this.dataset.itemcategory);">Beds</div>
                <div class="option" data-itemcategory="cat2" onclick="showCat(this.dataset.itemcategory);">Chairs</div>
            </div>

            <div id="cat1" class="category">
                @foreach($guitars as $guitar)
                    <div class="show-prod">
                        <img id="{{$guitar->id}}-img" src="{{$guitar->pic}}">
                        <h2 id="{{$guitar->id}}">{{$guitar->name}}
                            @if ($guitar->stock===0)
                                <span class="stock">(Out of Stock)</span>
                            @else
                                <span class="stock">(<b>{{$guitar->stock}}</b> in Stock)</span>
                            @endif
                        </h2>
                        <h3>₱ <span id="{{$guitar->id}}-prc">{{$guitar->prize}}</span></h3>
                        <p id="{{$guitar->id}}-desc">
                            {{$guitar->description}}
                        </p>
                        <div class="add-btn">
                            <input id="{{$guitar->id}}-qty" type="number" min="0" value="0"
                                   @if ($guitar->stock===0)
                                   readonly
                                    @endif
                            />
                            <button data-id="{{$guitar->id}}" onclick='addItem(this.dataset.id,{{$guitar->stock}})'
                                    @if ($guitar->stock===0)
                                    disabled
                                    @endif
                            >
                                <i class="fa fa-cart-plus"></i> Add
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>

            <div id="cat2" class="category hide">
                @foreach($headsets as $headset)
                    <div class="show-prod">
                        <img id="{{$headset->id}}-img" src="{{$headset->pic}}">
                        <h2 id="{{$headset->id}}">{{$headset->name}}
                            @if ($headset->id===0)
                                <span class="stock">(Out of Stock)</span>
                            @else
                                <span class="stock">(<b>{{$headset->stock}}</b> in Stock)</span>
                            @endif
                        </h2>
                        <h3>₱ <span id="{{$headset->id}}-prc">{{$headset->prize}}</span></h3>
                        <p id="{{$headset->id}}-desc">
                            {{$headset->description}}
                        </p>
                        <div class="add-btn">
                            <input id="{{$headset->id}}-qty" type="number" min="0" value="0"/>
                            <button data-id="{{$headset->id}}" onclick='addItem(this.dataset.id)'><i
                                        class="fa fa-cart-plus"></i> Add
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div id="add-prod" class="group">
            <h1><i class="fa fa-shopping-cart"></i> Cart</h1>
            <div class="container">
                <div class="cart-content">
                    <table class="cart">
                        <thead>
                        <td>Item</td>
                        <td>Qty</td>
                        <td>Sub-total</td>
                        </thead>
                    </table>
                    <table id="cart-items" class="cart">
                        <tr>
                            <td>No Items</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="cart cart-ttl">
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td>₱ <span id="cart-totamt">0</span>.00</td>
                        </tr>
                    </table>
                    <div class="add-btn">
                        <span onclick="clearItems();">Clear Items</span>
                        <button onclick='showCartCO("order");'><i class="fa fa-cart-arrow-down"></i> Checkout</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="cart-checkout" class="group">
            <h1><i class="fa fa-shopping-cart"></i> Cart Checkout</h1>
            <div class="container">
                <div class="cart-content">
                    <h2>Your Order</h2>
                    <table class="cart">
                        <thead>
                        <td>Item</td>
                        <td>Qty</td>
                        <td>Sub-total</td>
                        </thead>
                    </table>
                    <table id="cart-co-items" class="cart">
                        <tr>
                            <td>No Items</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="cart cart-ttl">
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td>₱ <span id="cart-co-totamt">0</span>.00</td>
                        </tr>
                    </table>
                    <h2>Account Info</h2>
                    <table class="cart-userinfo">
                        <tr>
                            <td>Name:</td>
                        </tr>
                        <tr>
                            <td>{{$user->profile->name}}</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                        </tr>
                        <tr>
                            <td>{{$user->profile->address}}</td>
                        </tr>
                        <tr>
                            <td>Contact Number</td>
                        </tr>
                        <tr>
                            <td>{{$user->profile->contactnumber}}</td>
                        </tr>
                        <tr>
                            <td>Email Address</td>
                        </tr>
                        <tr>
                            <td>{{$user->email}}</td>
                        </tr>
                    </table>
                    <h2>Choose Payment</h2>
                    <input type="radio" name="r-p" id="o-fp" class="payCB" value="full"  checked="checked"><label for="o-fp">Full Payment</label>
                    <input type="radio" name="r-p" id="o-pp" class="payCB" value="partial"><label for="o-pp">Partial Payment</label>
                    <div class="add-btn">
                        <button id="cBtn" onclick='hideCartCO("")'><i class="fa fa-thumbs-o-down"></i> Cancel</button>
                        <button id="oBtn" onclick='order()'><i class="fa fa-thumbs-o-up"></i> Proceed</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="cart-ty" class="group">
            <h1><i class="fa fa-thumbs-up"></i> Thank You</h1>
            <div class="container">
                <p>Your order was successfully processed.</p>
                <hr>
                <p>Order Number <b id="ordernum"></b></p>
                <a href="/home">
                    <button><i class="fa fa-book"></i> View</button>
                </a>
            </div>
        </div>

        <div id="reserve" class="group">
            <h1><i class="fa fa-calendar"></i> Studio Reservation</h1>
            <div class="container">
                <h2>Select Date</h2>
                <form name="form" method="post" action="/reservation" onsubmit="return validateForm()">
                    <input name="date" type="date" id="date" value="<?php echo date('Y-m-d'); ?>"
                           min="<?php echo date('Y-m-d'); ?>"
                           onchange="changeDate();"/>
                    <h2>Select Time</h2>
                    <div id="calendar">
                        <table>
                            <thead>
                            <td>8AM</td>
                            <td>9AM</td>
                            <td>10AM</td>
                            <td>11AM</td>
                            <td>12PM</td>
                            <td>1PM</td>
                            <td>2PM</td>
                            <td>3PM</td>
                            <td>4PM</td>
                            <td>5PM</td>
                            <td>6PM</td>
                            <td>7PM</td>
                            <td>8PM</td>
                            <td>9PM</td>
                            <td>10PM</td>
                            <td>11PM</td>
                            </thead>
                            <tr>
                                <td><input type="checkbox" name="time" id="time1" data-selection="0" data-sortorder="1"
                                           data-val="08:00" onclick="selectTime(this.id)"><label for="time1"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time2" data-selection="0" data-sortorder="2"
                                           data-val="09:00" onclick="selectTime(this.id)"><label for="time2"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time3" data-selection="0" data-sortorder="3"
                                           data-val="10:00" onclick="selectTime(this.id)"><label for="time3"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time4" data-selection="0" data-sortorder="4"
                                           data-val="11:00" onclick="selectTime(this.id)"><label for="time4"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time5" data-selection="0" data-sortorder="5"
                                           data-val="12:00" onclick="selectTime(this.id)"><label for="time5"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time6" data-selection="0" data-sortorder="6"
                                           data-val="13:00" onclick="selectTime(this.id)"><label for="time6"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time7" data-selection="0"
                                           data-sortorder="7" data-val="14:00" onclick="selectTime(this.id)"><label
                                            for="time7"></label></td>
                                <td><input type="checkbox" name="time" id="time8" data-selection="0" data-sortorder="8"
                                           data-val="15:00" onclick="selectTime(this.id)"><label for="time8"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time9" data-selection="0" data-sortorder="9"
                                           data-val="16:00" onclick="selectTime(this.id)"><label for="time9"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time10" data-selection="0"
                                           data-sortorder="10"
                                           data-val="17:00" onclick="selectTime(this.id)"><label for="time10"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time11" data-selection="0"
                                           data-sortorder="11" data-val="18:00" onclick="selectTime(this.id)"><label
                                            for="time11"></label></td>
                                <td><input type="checkbox" name="time" id="time12" data-selection="0"
                                           data-sortorder="12"
                                           data-val="19:00" onclick="selectTime(this.id)"><label for="time12"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time13" data-selection="0"
                                           data-sortorder="13"
                                           data-val="20:00" onclick="selectTime(this.id)"><label for="time13"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time14" data-selection="0"
                                           data-sortorder="14"
                                           data-val="21:00" onclick="selectTime(this.id)"><label for="time14"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time15" data-selection="0"
                                           data-sortorder="15"
                                           data-val="22:00" onclick="selectTime(this.id)"><label for="time15"></label>
                                </td>
                                <td><input type="checkbox" name="time" id="time16" data-selection="0"
                                           data-sortorder="16"
                                           data-val="23:00" onclick="selectTime(this.id)"><label for="time16"></label>
                                </td>
                            </tr>
                        </table>
                        <div class="legend">
                            <span class="red"></span>Occupied
                            <span class="grey"></span>Not Applicable
                        </div>
                        <hr>
                        <table class="tbl-time">
                            <thead>
                            <td>Start Time</td>
                            <td>End Time</td>
                            <td>Price</td>
                            </thead>
                            <tr>
                                <td><input name="starttime" type="text" id="st" value="" readonly></td>
                                <td><input name="endtime" type="text" id="et" value="" readonly></td>
                                <td><input name="prize" type="text" id="pr" value="" readonly></td>
                            </tr>
                        </table>
                    </div>
                        <h2>Choose Payment</h2>
                        <input type="radio" name="r-p" id="r-fp" class="payCB" value="partial" checked="checked"><label for="r-fp">Partial Payment</label>
                        <div class="add-btn">
                            <button type="button" onclick='showGroup("reserve-list")'><i class="fa fa-thumbs-o-down"></i> Cancel
                            </button>
                            <button><i class="fa fa-thumbs-o-up"></i> Proceed</button>
                        </div>
                </form>
            </div>
        </div>

        <div id="reserve-list" class="group">
            <h1><i class="fa fa-calendar"></i> Reservations</h1>
            <div class="container">
                <table>
                    <thead>
                    <td>Reservation #</td>
                    <td>Date</td>
                    <td>Start Time</td>
                    <td>End Time</td>
                    </thead>
                    <tbody>
                        @foreach($reservation as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->date}}</td>
                                <td>{{$item->starttime}}</td>
                                <td>{{$item->endtime}}</td>
                            </tr>
                        @endforeach
                        @if(count($reservation) === 0)
                            <tr>
                                <td>N/A</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class="add-btn">
                    <button onclick='showGroup("reserve")'><i class="fa fa-book"></i> Reserve Now</button>
                </div>
            </div>
        </div>

        <div id="build" class="group">
            <h1>
                <i class="fa fa-puzzle-piece"></i> Build
                <span class="printBtn" onclick='printPage("Guitar")'>
                    <i class="fa fa-print"></i>
                </span>
            </h1>
            <canvas id="template" width="836" height="300"></canvas>
            <script type="text/javascript">
                var tmp = document.getElementById("template");
                var ctx = tmp.getContext('2d');
                var images;
                var totprice = 0;

                function loadImages(sources, callback) {
                    images = {};
                    var loadImages = 0;
                    var numImages = 0;

                    for (var src in sources) {
                        numImages++;
                    }
                    for (var src in sources) {
                        images[src] = new Image();
                        images[src].onload = function () {
                            if (++loadImages >= numImages);
                            callback(images);
                        };
                        images[src].src = sources[src];
                    }
                }

                var sources = {
                    overlay: "/imgs/customs/Overlay.png",
                    overlay_paddle: "/imgs/customs/Overlay_Paddle.png",
                    overlay_satin: "/imgs/customs/Overlay_Satin.png",
                    Default: "/imgs/customs/Natural.png",
                    Black: "/imgs/customs/Black.png",
                    Red: "/imgs/customs/Red.png",
                    Orange: "/imgs/customs/Orange.png",
                    Green: "/imgs/customs/Green.png",
                    SonicBlue: "/imgs/customs/SonicBlue.png",
                    headstock_Default: "/imgs/customs/Headstock_Natural.png",
                    headstock_Black: "/imgs/customs/Headstock_Black.png",
                    headstock_Red: "/imgs/customs/Headstock_Red.png",
                    headstock_Orange: "/imgs/customs/Headstock_Orange.png",
                    headstock_Green: "/imgs/customs/Headstock_Green.png",
                    headstock_SonicBlue: "/imgs/customs/Headstock_SonicBlue.png",
                    stopTail: "/imgs/customs/Stop_Tail.png",
                    mountingRings: "/imgs/customs/Mounting_Rings.png",
                    knobs: "/imgs/customs/Knobs.png",
                    switch: "/imgs/customs/Switch.png",
                    tuners: "/imgs/customs/Tuners.png",
                    fretboard_Default: "/imgs/customs/Fretboard_Rosewood.png",
                    fretboard_Maple: "/imgs/customs/Fretboard_Maple.png",
                    fretboard_Rosewood: "/imgs/customs/Fretboard_Rosewood.png",
                    fretboard_Rosewood_blocks: "/imgs/customs/Fretboard_Rosewood_Blocks.png",
                    pickguard_Default: "/imgs/customs/None.png",
                    pickguard_White: "/imgs/customs/Pickguard_White.png",
                    pickguard_Black: "/imgs/customs/Pickguard_Black.png",
                    pickguard_Red: "/imgs/customs/Pickguard_Red.png",
                    pickguard_Blue: "/imgs/customs/Pickguard_Blue.png",
                    pickguard_Yellow: "/imgs/customs/Pickguard_Yellow.png",
                    bridge_Default: "/imgs/customs/Bridge_Chrome.png",
                    bridge_Chrome: "/imgs/customs/Bridge_Chrome.png",
                    bridge_Gold: "/imgs/customs/Bridge_Gold.png",
                    neck_Default: "/imgs/customs/Neck_Chrome.png",
                    neck_Chrome: "/imgs/customs/Neck_Chrome.png",
                    neck_Gold: "/imgs/customs/Neck_Gold.png"
                }

                loadImages(sources, function (images) {
                    ctx.drawImage(images.Default, 0, 4);
                    ctx.drawImage(images.headstock_Default, 692, 94);
                    ctx.drawImage(images.stopTail, 108, 97);
                    ctx.drawImage(images.mountingRings, 171, 100);
                    ctx.drawImage(images.bridge_Default, 170, 110);
                    ctx.drawImage(images.neck_Default, 250, 110);
                    ctx.drawImage(images.fretboard_Default, 296, 114);
                    ctx.drawImage(images.overlay, 0, 0);
                    ctx.drawImage(images.overlay_paddle, 797, 90);
                    ctx.drawImage(images.pickguard_Default, 167, 165);
                    ctx.drawImage(images.knobs, 80, 195);
                    ctx.drawImage(images.switch, 103, 198);
                    ctx.drawImage(images.tuners, 719, 88);
                    ctx.drawImage(images.overlay_satin, -10, -10);
                    ctx.fillStyle = "red";
                    ctx.font = "20px Oswald, sans-serif";
                    totprice = parseInt(document.getElementById("srvcChrg").innerHTML);
                    ctx.fillText("₱" + totprice + ".00", 750, 20);
                });

                function reDraw() {
                    var c1 = document.querySelector('input[name="body"]:checked');
                    var c2 = document.querySelector('input[name="pickguard"]:checked');
                    var c3 = document.querySelector('input[name="fretboard"]:checked');
                    var c4 = document.querySelector('input[name="bridge"]:checked');
                    var baseprice = parseInt(document.getElementById("srvcChrg").innerHTML);
                    totprice = baseprice + parseInt(c1.dataset.price) + parseInt(c2.dataset.price) + parseInt(c3.dataset.price) + parseInt(c4.dataset.price)
                    ctx.drawImage(images[c1.value], 0, 4);
                    ctx.drawImage(images["headstock_" + c1.value], 692, 94);
                    ctx.drawImage(images.stopTail, 108, 97);
                    ctx.drawImage(images.mountingRings, 171, 100);
                    ctx.drawImage(images["bridge_" + c4.value], 170, 110);
                    ctx.drawImage(images["neck_" + c4.value], 250, 110);
                    ctx.drawImage(images["fretboard_" + c3.value], 296, 114);
                    ctx.drawImage(images.overlay, 0, 0);
                    ctx.drawImage(images.overlay_paddle, 797, 90);
                    ctx.drawImage(images["pickguard_" + c2.value], 167, 165);
                    ctx.drawImage(images.knobs, 80, 195);
                    ctx.drawImage(images.switch, 103, 198);
                    ctx.drawImage(images.tuners, 719, 88);
                    ctx.drawImage(images.overlay_satin, -10, -10);
                    ctx.font = "20px Oswald, sans-serif";
                    ctx.fillText("₱" + totprice + ".00", 750, 20);
                }
            </script>
            <div class="custom-selection">
                <div class="custom-group">
                    <h2>Body</h2>
                    <ul>
                        @foreach($bodies as $body)
                            <li>
                                <input onchange="reDraw()" type="radio" name="body"
                                        id="body{{$body->id}}"
                                        value="{{$body->name}}"
                                        data-price="{{$body->prize}}"
                                        @if ($body->name == "Default")
                                            checked
                                        @endif>
                                <label for="body{{$body->id}}">
                                    <span class="preview" style='background-image: url("{{$body->imgs}}")'></span>
                                    {{$body->name}} <b>₱{{$body->prize}}</b>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Pickguard</h2>
                    <ul>
                        @foreach($pickguards as $pickguard)
                            <li>
                                <input onchange="reDraw()" type="radio" name="pickguard"
                                        id="pickguard{{$pickguard->id}}"
                                        value="{{$pickguard->name}}"
                                        data-price="{{$pickguard->prize}}"
                                        @if ($pickguard->name == "Default")
                                            checked
                                        @endif>
                                <label for="pickguard{{$pickguard->id}}">
                                    <span class="preview" style='background-image: url("{{$pickguard->imgs}}")'></span>
                                    {{$pickguard->name}} <b>₱{{$pickguard->prize}}</b>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Fretboard</h2>
                    <ul>
                        @foreach($fretboards as $fretboard)
                            <li>
                                <input onchange="reDraw()" type="radio" name="fretboard"
                                        id="fretboard{{$fretboard->id}}" value="{{$fretboard->name}}"
                                        data-price="{{$fretboard->prize}}"
                                        @if ($fretboard->name == "Default")
                                            checked
                                        @endif>
                                <label for="fretboard{{$fretboard->id}}">
                                    <span class="preview" style='background-image: url("{{$fretboard->imgs}}")'></span>
                                    {{$fretboard->name}} <b>₱{{$fretboard->prize}}</b>
                                </label>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <div class="custom-group">
                    <h2>Bridge</h2>
                    <ul>
                        @foreach($bridges as $bridge)
                            <li>
                                <input onchange="reDraw()" type="radio" name="bridge" id="bridge{{$bridge->id}}"
                                        value="{{$bridge->name}}" data-price="{{$bridge->prize}}"
                                        @if ($bridge->name == "Default")
                                            checked
                                        @endif>
                                <label for="bridge{{$bridge->id}}">
                                    <span class="preview" style='background-image: url("{{$bridge->imgs}}")'></span>
                                    {{$bridge->name}} <b>₱{{$bridge->prize}}</b>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <p class="note">Inclusive of ₱<span id="srvcChrg">250</span>
                service charge.</p>
            <div class="add-btn">
                <button type="button" onclick='showCartCO("custom")'><i class="fa fa-cart-plus"></i> Checkout</button>
            </div>
        </div>
    </div>
</div>

<div id="print">
    <div id="OR" class="OR">
        KnoxVille<br>
        654 Quirino Highway, Bagbag, Novaliches, Quezon City, Philippines<br>
        0917-851-7727<br>
        <br>
        <span id="print-order-date">00/00/0000</span><br>
        Order Number<br>
        <span id="print-ordernum">00000000</span>
        <hr>
        Customer Information<br>
        <div class="align-left">ID: <span id="print-userid">00000000</span><br>
            Email: <span id="print-email">user@gmail.com</span>
            <hr>
            <table id="print-or-t"><!--
    <tr>
        <td>product1</td>
        <td>0.00</td>
    </tr>
    <tr>
        <td>product2</td>
        <td>0.00</td>
    </tr> -->
            </table>
            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Amount Due</td>
                    <td>P<span id="print-amount">0</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cash</td>
                    <td>P<span id="print-cash">0</span></td>
                </tr>
                <tr>
                    <td>Change</td>
                    <td>P<span id="print-change">0</span>.00</td>
                </tr>
            </table>
            <hr>
        </div>
        This serves as your official Receipt.
        Thank you, and please come again.
    </div>
    <div id="Guitar">
        <img id="cnvsImg">
    </div>
</div>

</body>

</html>