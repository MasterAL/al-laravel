<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>JLB | Register</title>
    <link rel="icon" href="img/pro.jpg" type="image/png">
    <link href="css/form.css" rel="stylesheet" type="text/css">
    <link href="css/font.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">

</head>

<body>

<div class="container">

    <div class="head">
        <span id="logo"></span>
        <span id="logo-link">JLBWoodart</span>
    </div>

    <form class="form" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <h2><i class="fa fa-user"></i> Create account</h2>
        <input type="text" class="form-control" placeholder="Name" name="name" value="" required="">
        <input type="email" placeholder="Email Address" name="email" value="" required="">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input type="text" class="form-control" placeholder="Delivery Address" name="location" value="" required="">
        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
        <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" value="" required="">
        @if ($errors->has('contact_number'))
            <span class="help-block">
                <strong>{{ $errors->first('contact_number') }}</strong>
            </span>
        @endif
        <input type="password" placeholder="Password" name="password" required="">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation"
               required="">
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
        <div class="tc">
            <input type="checkbox" name="termsandconditions" required="">
            I agree to the <a href="" class="link" onclick='window.open("website-terms-and-conditions.doc")'>Terms & Conditions</a>.
        </div>
        <button type="submit">Register</button>
    </form>

    <div class="foot">
        <a href="/login" class="link">click here if you already have an account</a>
    </div>
</div>

</body>

</html>

