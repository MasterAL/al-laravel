<?php

use Illuminate\Database\Seeder;
use App\Custom;

class CustomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('custom')->delete();
        Custom::create(array('type' => 'Body', 'name' => 'Default', 'prize' => 100, 'imgs' => '/imgs/customs/Natural.png', 'enable' => 1));
        Custom::create(array('type' => 'Body', 'name' => 'Black', 'prize' => 100, 'imgs' => '/imgs/customs/Black.png', 'enable' => 1));
        Custom::create(array('type' => 'Body', 'name' => 'Green', 'prize' => 100, 'imgs' => '/imgs/customs/Green.png', 'enable' => 1));
        Custom::create(array('type' => 'Body', 'name' => 'Red', 'prize' => 100, 'imgs' => '/imgs/customs/Red.png', 'enable' => 1));
        Custom::create(array('type' => 'Body', 'name' => 'Orange', 'prize' => 100, 'imgs' => '/imgs/customs/Orange.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'Default', 'prize' => 0, 'imgs' => '/imgs/customs/None.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'White', 'prize' => 50, 'imgs' => '/imgs/customs/Pickguard_White.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'Black', 'prize' => 50, 'imgs' => '/imgs/customs/Pickguard_Black.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'Red', 'prize' => 50, 'imgs' => '/imgs/customs/Pickguard_Red.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'Yellow', 'prize' => 50, 'imgs' => '/imgs/customs/Pickguard_Yellow.png', 'enable' => 1));
        Custom::create(array('type' => 'Pickguard', 'name' => 'Blue', 'prize' => 50, 'imgs' => '/imgs/customs/Pickguard_Blue.png', 'enable' => 1));
        Custom::create(array('type' => 'Fretboard', 'name' => 'Rosewood', 'prize' => 400, 'imgs' => '/imgs/customs/Fretboard_Rosewood.png', 'enable' => 1));
        Custom::create(array('type' => 'Fretboard', 'name' => 'Maple', 'prize' => 300, 'imgs' => '/imgs/customs/Fretboard_Maple.png', 'enable' => 1));
        Custom::create(array('type' => 'Bridge', 'name' => 'Chrome', 'prize' => 80, 'imgs' => '/imgs/customs/Bridge_Chrome.png', 'enable' => 1));
        Custom::create(array('type' => 'Bridge', 'name' => 'Gold', 'prize' => 80, 'imgs' => '/imgs/customs/Bridge_Gold.png', 'enable' => 1));

    }
}
