<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->delete();
        Product::create(array('name' => 'CORT CGP X-1', 'prize' => 10150.00, 'pic' => 'imgs/products/8541893_orig.png', 'category' => 'Guitar', 'description' => 'This powerful combination of passive pickups has the power of EMG\'s active 81 with the soul of a passive PAF', 'stock' => 10));
        Product::create(array('name' => 'CORT EVL-K5', 'prize' => 15000.00, 'pic' => 'imgs/products/8541893_orig.png', 'category' => 'Guitar', 'description' => 'This powerful combination of passive pickups has the power of EMG\'s active 81 with the soul of a passive PAF', 'stock' => 10));
        Product::create(array('name' => 'CORT GR-200', 'prize' => 11900.00, 'pic' => 'imgs/products/6039596_orig.png', 'category' => 'Guitar', 'description' => 'The G200 features a double cutaway body for comfort and easy access to the higher frets.', 'stock' => 10));
        Product::create(array('name' => 'Superlux HD661', 'prize' => 1700.00, 'pic' => 'imgs/products/4931295_orig.jpg', 'category' => 'Headset', 'description' => 'The HD661 is a closed-back, circumaural headphone which is excellent for live sound monitoring application because of its sound profile of depth of sound field. It\'s also suitable for studio and broadcast. Although suitable for a very wide range of applications, the exceptional attenuation of external noise of the HD 661 makes it particularly useful for use in a high noise environment.','stock' => 10));
        Product::create(array('name' => 'Superlux HD631', 'prize' => 2400.00, 'pic' => 'imgs/products/5549865_orig.jpg', 'category' => 'Headset', 'description' => 'The HD631 series is a high sound quality headphone developed specifically for professional DJs, designed to clearly emphasize the beat of the music as well as capturing its more subtle nuances. With its 51 mm (2”) extra-large diaphragms and high-energy Neodymium magnets, the HD631 series presents an excellent, balanced sound profile while giving an extra punch to the low end frequencies. Even the most critical professional DJ will find the HD631 series ideal for delivering the exact sound performance they need to do their job.','stock' => 10));
    }
}
