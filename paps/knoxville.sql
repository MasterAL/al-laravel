-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2016 at 11:39 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `knoxville`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom`
--

CREATE TABLE IF NOT EXISTS `custom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bodyType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bodyColor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fretboard` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headstockColor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customization`
--

CREATE TABLE IF NOT EXISTS `customization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `customizationid` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customization_userid_foreign` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customize`
--

CREATE TABLE IF NOT EXISTS `customize` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `customid` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customize_userid_foreign` (`userid`),
  KEY `customize_customid_foreign` (`customid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_08_22_124740_reservation', 1),
('2016_08_22_124926_product', 1),
('2016_08_22_125234_profile', 1),
('2016_08_22_125341_order', 1),
('2016_08_22_125741_custom', 2),
('2016_08_22_131106_customize', 2);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `productid` int(10) unsigned NOT NULL,
  `ordernum` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `change` decimal(8,2) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_userid_foreign` (`userid`),
  KEY `order_productid_foreign` (`productid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `userid`, `productid`, `ordernum`, `change`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 5, '2016082801', '500.00', 'Ongoing', '2016-08-28 01:21:10', '2016-08-28 01:21:10'),
(5, 2, 5, '2016082802', '15000.00', 'Ongoing', '2016-08-28 01:36:06', '2016-08-28 01:36:06'),
(6, 2, 5, '2016082803', '11111.00', 'Ongoing', '2016-08-28 01:37:06', '2016-08-28 01:37:06'),
(7, 2, 5, '2016082804', '11111.00', 'Ongoing', '2016-08-28 01:37:31', '2016-08-28 01:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prize` decimal(8,2) NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(522) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `prize`, `pic`, `category`, `description`, `created_at`, `updated_at`) VALUES
(5, 'CORT CGP X-1', '10150.00', 'imgs/products/8541893_orig.png', 'Guitar', 'This powerful combination of passive pickups has the power of EMG''s active 81 with the soul of a passive PAF', '2016-08-28 00:02:36', '2016-08-28 00:02:36'),
(6, 'CORT EVL-K5', '15000.00', 'imgs/products/8541893_orig.png', 'Guitar', 'This powerful combination of passive pickups has the power of EMG''s active 81 with the soul of a passive PAF', '2016-08-28 00:02:36', '2016-08-28 00:02:36'),
(7, 'CORT GR-200', '11900.00', 'imgs/products/6039596_orig.png', 'Guitar', 'The G200 features a double cutaway body for comfort and easy access to the higher frets.', '2016-08-28 00:02:36', '2016-08-28 00:02:36'),
(8, 'Superlux HD661', '1700.00', 'imgs/products/4931295_orig.jpg', 'Headset', 'The HD661 is a closed-back, circumaural headphone which is excellent for live sound monitoring application because of its sound profile of depth of sound field. It''s also suitable for studio and broadcast. Although suitable for a very wide range of applications, the exceptional attenuation of external noise of the HD 661 makes it particularly useful for use in a high noise environment.', '2016-08-28 00:02:36', '2016-08-28 00:02:36'),
(9, 'Superlux HD631', '2400.00', 'imgs/products/5549865_orig.jpg', 'Headset', 'The HD631 series is a high sound quality headphone developed specifically for professional DJs, designed to clearly emphasize the beat of the music as well as capturing its more subtle nuances. With its 51 mm (2”) extra-large diaphragms and high-energy Neodymium magnets, the HD631 series presents an excellent, balanced sound profile while giving an extra punch to the low end frequencies. Even the most critical professional DJ will find the HD631 series ideal for delivering the exact sound performance they need to do ', '2016-08-28 00:02:36', '2016-08-28 00:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactnumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_userid_foreign` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `userid`, `name`, `address`, `contactnumber`, `created_at`, `updated_at`) VALUES
(5, 8, 'langdeleon', 'bulacan', '123465', '2016-08-27 21:46:51', '2016-08-27 21:46:51');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_userid_foreign` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isadmin` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `isadmin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', '$2y$10$dBVoJs7LuKa7lXhroGNTje/4L.HdlItsHzlnEtC7j04pkJ0dddooa', 1, '0DjruvjiP11m5Vkgg7V2Xb5r1kMoujYNh2SAaDDzr48qRKWtcmVoLIWoVhwK', '2016-08-27 19:40:43', '2016-08-28 00:42:42'),
(2, 'user@gmail.com', '$2y$10$fkFF8B1rsLwuKsKCBjVs2e5B1NBqt8Fu0Gxgp3n5hihKji32h2ww.', 0, 'YidNuyKrJMhvxaWKLq2z0h2wqC4LjfyFeDJSWVqREvGNcqnx47unzZgFSPrA', '2016-08-27 19:40:43', '2016-08-28 01:38:40'),
(8, 'langdeleon@gmail.com', '$2y$10$HN4sFq.9IfFU.sUhW8dAxun6Pv7Y1RKtvtMfdtNFEMlAFTVjz7mFy', 0, 'hLgSeplbHYsKe7zmMmnO96l6ppYBjwKwSDd7F7U5ywVSuHSMaaY74N6v86EP', '2016-08-27 21:46:51', '2016-08-27 21:53:13');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customization`
--
ALTER TABLE `customization`
  ADD CONSTRAINT `customization_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `customize`
--
ALTER TABLE `customize`
  ADD CONSTRAINT `customize_customid_foreign` FOREIGN KEY (`customid`) REFERENCES `custom` (`id`),
  ADD CONSTRAINT `customize_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `order_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
